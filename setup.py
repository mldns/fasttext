#!/usr/bin/env python
############################################################
# Setup script.
############################################################
# Install requirements.
############################################################

from setuptools import setup


__version__ = '0.0.1'


setup(
    name='mldns',
    version=__version__,
    #author='',
    #author_email='',
    url='https://gitlab.com/mldns/fasttext',
    description='Leveraging DNS and Machine Learning to protect Home Networks',
    long_description='DNS (Domain Name Service) is a key Internet service. DNS also offers great opportunities as a security tool, and the potential is not yet fully exploited. Our approach is to protect home networks by analyzing the DNS traffic using machine learning techniques and to block malicious domains.',
    #extras_require={'test': 'pytest'},
    install_requires=[
        'setuptools>=42',
        'fasttext @ git+ssh://git@gitlab.com/mldns/facebookresearch/fasttext@master',
        'numpy',
        'pandas',
        'sklearn',
        'matplotlib',
        'tld',
        'mlflow',
        'boto3',
        'configparser',
        'simple-term-menu',
    ],
)
