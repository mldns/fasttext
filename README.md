# MLDNS - FastText

This repository is part of the [Machine Learning DNS (MLDNS)](https://gitlab.com/mldns/mldns) research project and mainly focuses on the machine learning aspects to classify domain names into benign and malicious.

The report to this repository can be found [here](https://gitlab.com/mldns/ml-report).

The developer documentation is available [here](https://mldns.gitlab.io/fasttext).


## Introduction

Home network security is a big problem that is not easy to solve. Every year, users are affected
by various attacks. Most of these attacks can be mitigated by blocking malicious domains.
Currently, all unknown domains are considered benign accepted. This is precisely where we want
to intervene.

Our research target is to determine whether machine learning can be used to distinguish benign
and malicious domains based on its name. We want to show that it is possible to train a model
that is small and efficient enough to be put into a home router by default.

Surprisingly, our experiments clearly show that an appropriate classification based only on the domain name
is very promising. Depending on the machine learning tool, the resulting models are also
relative small and efficient, so that they can be easily integrated into any home router.

In this project different models were trained using [fastText](https://fasttext.cc/) and [scikit-learn](https://scikit-learn.org/).


## Installation

```bash
# download project and fasttext
git clone --depth=1 git@gitlab.com:mldns/fasttext.git
cd fasttext

# create and activate virtual environment
python3 -m venv .venv
source .venv/bin/activate

# install packages
pip install -U pip
pip install .

# download data (this may take some time)
mkdir ./datasets
./scripts/download_and_prepare_white_and_blacklist.py --out-dir ./datasets
./scripts/advanced_whitelist_cleaning.py
```


## Train your first model

To test the installation, just train a very simple basic model.
This basic model may not achieve the best results but should verify that all requirements are satisfied.
For more examples you may want to look inside `./scripts/train_all.sh`.

```bash
# train (this may take several minutes)
./train.py \
    --fasttext dim=5 \
    --out ./out/5d

# create plots and documentation
./scripts/create_model_plots.py --automatic
./scripts/write_org_file.sh

# play around
./tryout.py
#  > FastText model (*.bin): ./out/5d/fasttext-model.bin
#  > PCA model is exists (*.pkl): <ENTER>
#  > Domain to predict: google.com
#  ...
#  > Domain to predict: <ENTER>
```

In this project, all models and evaluations are stored using [MLflow](https://github.com/mlflow/mlflow/).

To access these results, run `mlflow ui`.
The server address of mlflow is displayed in the terminal, the default value is `http://127.0.0.1:5000`.


You might want to look into the help page.

```
usage: train.py [-h] [--out OUT] [--out-name OUT_NAME] [--seed SEED]
                [--data-selection DATA_SELECTION]
                [--data-preprocessing DATA_PREPROCESSING [DATA_PREPROCESSING ...]]
                [--whitelist_path WHITELIST_PATH]
                [--blacklist_path BLACKLIST_PATH] [--dga_path DGA_PATH]
                [--fasttext [FASTTEXT ...]] [--pca [PCA ...]] [--svm [SVM ...]]
                [--fasttext-model FASTTEXT_MODEL] [--pca-model PCA_MODEL]
                [--svm-model SVM_MODEL] [--fasttext-threshold FASTTEXT_THRESHOLD]
                [--pca-threshold PCA_THRESHOLD] [--svm-threshold SVM_THRESHOLD]
                [--fasttext-word-vector-scaling FASTTEXT_WORD_VECTOR_SCALING]
                [--pca-output-scaling PCA_OUTPUT_SCALING]

Train models.

options:
  -h, --help            show this help message and exit
  --out OUT             output directory [Default: "./out/yyyymmdd-hhmmss"]
  --out-name OUT_NAME   output base filename prefix
  --seed SEED           Set fix seed instead of random seed.
  --data-selection DATA_SELECTION
                        Data selection steps [Default: whitelist_blacklist]
  --data-preprocessing DATA_PREPROCESSING [DATA_PREPROCESSING ...]
                        Data preprocessing steps [Default:
                        lower_case_split_by_dot_dash_underscore]
  --whitelist_path WHITELIST_PATH
                        Path to the whitelist file [default:
                        ./datasets/whitelist_cleaned.txt]
  --blacklist_path BLACKLIST_PATH
                        Path to the whitelist file [default:
                        ./datasets/blacklist.txt]
  --dga_path DGA_PATH   Path to the DGA file [default:
                        ./datasets/dgas_preprocessed.csv]
  --fasttext [FASTTEXT ...]
                        FastText training parameters
  --pca [PCA ...]       PCA training parameters
  --svm [SVM ...]       SVM training parameters
  --fasttext-model FASTTEXT_MODEL
                        path to fasttext model file
  --pca-model PCA_MODEL
                        path to pca model file
  --svm-model SVM_MODEL
                        path to svm model file
  --fasttext-threshold FASTTEXT_THRESHOLD
                        Fasttext threshold [Default: None]
  --pca-threshold PCA_THRESHOLD
                        PCA threshold [Default: None]
  --svm-threshold SVM_THRESHOLD
                        SVM threshold [Default: None]
  --fasttext-word-vector-scaling FASTTEXT_WORD_VECTOR_SCALING
                        Scale word vectors/FastText output [Default: no_scaling]
  --pca-output-scaling PCA_OUTPUT_SCALING
                        Scale PCA output [Default: no_scaling]
```


## Data

Below you will find a rather short section of the data description.
For more information and a deeper understanding, you should take a look at the report, which you can find [here](https://gitlab.com/mldns/ml-report).

### Whitelist Data
The Cisco Umbrella Popularity List.  
This list consists of the most popular 1
million domains, which are the most queried domains in the Umbrella global network.  
https://s3-us-west-1.amazonaws.com/umbrella-static/index.html

### Blacklist Data
- COVID-19 Cyber Threat Coalition Blocklist  
Blocklist of domains which do malicious or criminal activies in the COVID-19 pandemic.  
The complete blacklist can be found here:
https://blocklist.cyberthreatcoalition.org/vetted/domain.txt
- Steven Black’s Hosts file  
uses the most basic variant Unified hosts which includes adware and malware.
The full list of included sources can be found here:
https://github.com/StevenBlack/hosts#sources-of-hosts-data-unified-in-this-variant  
The complete blacklist can be found here:
https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
- Universite Toulouse 1 Capitolé Blacklist  
This project uses the categories malware and phishing.  
All blacklists sorted into multiple categories can be found here:
http://dsi.ut-capitole.fr/blacklists/index_en.php

### Datasets
This projects uses the above domain lists to generate three datasets.

Datasets:
- whitelist_blacklist  - get the same amount of data for both whitelist and blacklist (as much as the minimum of both lists)
- whitelist_blacklist_unbalanced - take as many of each whitelist and blacklist as possible (unbalanced)
- whitelist_blacklist_cutted - get the same amount of data of both lists but cutted to a value default: 100,000

The following preprocess steps are used.

- remove duplicate domains,
- remove invalid top level domains (TLDs),
- remove domains that are also part of the blacklist,
- remove local domain addresses, e.g. localhost,
- remove domains that include UUID as a significant part of the domain,
- remove domains that include subdomains with more than 30 characters

To use a different dataset you have to set the `--data-selection` option of `train.py`.

There is also an option to remove DGA (Domain Generation Algorithm) domains.  
For this you need a DGA list which you preprocess by replacing the `_` , `.` , `-` with a space character.  
You place this preprocessed file in `./datasets/dgas_preprocessed.csv` before executing `advanced_whitelist_cleaning.py`.

For Datasets with DGA you can choose one of the following options.
- whitelist_blacklist_and_dga - same as whitelist_blacklist but with removing dga domains
- whitelist_blacklist_and_dga_cutted - same as whitelist_blacklist_cutted but with removing dga domains


## 3rd Party Licenses

The following Tools are used:

- [fastText](https://github.com/facebookresearch/fastText/blob/main/LICENSE)
- [Python](https://docs.python.org/3/license.html)
- [Matplotlib](https://matplotlib.org/stable/users/license.html)
- [NumPy](https://numpy.org/doc/stable/license.html)
- [SetupTools](https://github.com/pypa/setuptools/blob/main/LICENSE)
- [pandas](https://github.com/pandas-dev/pandas/blob/master/LICENSE)
- [sklearn](https://github.com/scikit-learn/scikit-learn/blob/main/COPYING)
- [tld](https://github.com/barseghyanartur/tld#license)
- [mlflow](https://github.com/mlflow/mlflow/blob/master/LICENSE.txt)
- [boto3](https://github.com/boto/boto3/blob/develop/LICENSE)
- [configparser](https://github.com/jaraco/configparser/blob/main/LICENSE)
- [simple-term-menu](https://github.com/IngoMeyer441/simple-term-menu/blob/develop/LICENSE)
- [pdoc](https://github.com/pdoc3/pdoc/blob/master/LICENSE.txt)  [only for developer documentation generation]
