#!/usr/bin/env python
############################################################
# Create model plots.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

from argparse import ArgumentParser

import fasttext
from sklearn.decomposition import PCA
from pickle import load
from os import makedirs, path, environ

import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

import numpy as np
import pandas as pd

from glob import iglob
import os

from executor import Executor
from settings import label, get_seed


def parseArgs():
    """
    Parse command-line arguments.

    Returns
    -------
    Namespace
        Run configuration.
        Converted argument strings as object with matching attributes.
    """
    parser = ArgumentParser(description='Create model plots.')

    parser.add_argument(
        '--model',
        type=str,
        default='fasttext-model.bin',
        help='Path to model file example \'model.bin\'',
    )

    parser.add_argument(
        '--pca',
        type=str,
        default='pca-model.pkl',
        help='Path to PCA file',
    )
    parser.add_argument(
        '--automatic',
        default=False,
        action='store_true',
        help='Automatically selects the last directory in ./out/',
    )

    config = parser.parse_args()
    return config



def load_models(model_path, pca_path):
    """
    Loads the Fasttext and PCA model.

    Arguments
    ---------
    model_path : str
        Path to Fasttext model (.bin)
    pca_path : str
        Path to PCA model (.pkl)

    Returns
    -------
    tuple
        Model and PCA
    """
    model = fasttext.load_model(model_path)
    pca = None
    if path.exists(pca_path):
        pca = load(open(pca_path, 'rb'))
    return model, pca


def prepare_domain(domain, latest_config):
    """
    Preprocessed the domain by using latest_config.

    The latest_config is the config which was used on the creation of the model.
    So, the same preprocessing steps are performed here.

    Arguments
    ---------
    domain : str
        Domain name.
    latest_config : dict
        Model training config.

    Returns
    -------
    any
        Preprocessed/Prepared domain
    """
    [[x]] = Executor.run_pipeline(latest_config.data_preprocessing, [domain])
    return x


def prepare(filename, model):
    """
    Reads a csv file into a DataFrame

    Arguments
    ---------
    filename : str
        Path to the CSV file
    model : object
        Fasttext model

    Returns
    -------
    pd.DataFrame
        Data frame with name,label and vectors as columns
    """
    df = pd.read_csv(filename, header=None, names=['name'])
    df[['label','name']] = df['name'].str.split(' ', 1, expand=True)
    df['name'] = df.name.str.replace(r'\s+', ' ', regex=True)
    df['vectors'] = df['name'].apply(model.get_sentence_vector)
    return df


def extract_samples_and_labels(df):
    """
    Extract the vectors and labels of a DataFrame.

    Arguments
    ---------
    df : pd.DataFrame
        Data frame to extract the values from

    Returns
    -------
    tuple
        Returns the vector list and the labels (as int)
    """
    X = np.array(df['vectors'].to_list())
    y = df['label'].values
    y[y == label['top']] = 0
    y[y == label['bad']] = 1
    y = np.asarray(y, dtype=int)
    return X, y


def fasttext_predict(domain, model):
    """
    Predict with the Fasttext model.

    Arguments
    ---------
    domain : str
        Domain
    model : object
        Fasttext model

    Returns
    -------
    float
        Probability
    """
    (predicted_label,), [probability] = model.predict(domain, k=1)
    return -probability if predicted_label == label['bad'] else probability


def predict_with_pcas(domain, model, pca_1d, pca_2d, latest_config):
    """
    Predict/Transform the Sentence vector to a lower dimension through PCA

    Both PCA's pca_1d and pca_2d models are used to create
    Dimension reduction to 1D and to 2D

    Arguments
    ---------
    domain : str
        Domain name.
    model : object
        Fasttext model
    pca_1d : object
        PCA model 1d (n_components=1)
    pca_2d : object
        PCA model 2d (n_components=2)
    latest_config : dict
        Latest Configuration

    Returns
    -------
    tuple
        pca_1d and pca_2d prediction/transformation
    """
    sentence_vector = [model.get_sentence_vector(prepare_domain(domain=domain, latest_config=latest_config))]
    _, sentence_vector = latest_config.fasttext_word_vector_scaling(sentence_vector)
    return pca_1d.transform(sentence_vector)[0], pca_2d.transform(sentence_vector)[0]



def savefig_1d_pca(
    top_validate_1d,
    bad_validate_1d,
    save_path,
    output_file="pca-1d.png",
):
    """
    Save the 1D plot.

    Arguments
    ---------
    top_validate_1d : np.ndarray
        whitelist dataset 1d
    bad_validate_1d : np.ndarray
        blacklist dataset 1d
    save_path : str
        path to the directory for the image
    output_file : str
        filename for the plot
    """
    plt.figure(figsize=(16, 1))
    ax = plt.subplot(111)
    ax.set_yticks([0,1])
    ax.set_yticks([-0.5,0,0.5,1,1.5], minor=True)
    ax.set_yticklabels(['Bad', 'Top'])
    ax.set_autoscaley_on(False)

    ax.plot(
        top_validate_1d,
        np.ones(len(top_validate_1d)),
        'ro',
        label='Top',
        alpha=0.05,
        markersize=1.5,
    )
    ax.plot(
        bad_validate_1d,
        np.zeros(len(bad_validate_1d)),
        'ko',
        label='Bad',
        alpha=0.15,
        markersize=1.5,
    )
    plt.savefig(path.join(save_path, output_file), bbox_inches='tight')


def savefig_2d_pca(
    top_validate_2d,
    bad_validate_2d,
    save_path,
    output_file="pca-2d.png",
):
    """
    Save the 2D plot.

    Arguments
    ---------
    top_validate_2d : np.ndarray
        whitelist dataset 2d
    bad_validate_2d : np.ndarray
        blacklist dataset 2d
    save_path : str
        path to the directory for the image
    output_file : str
        filename for the plot
    """
    fig = plt.figure(figsize=(10,10), constrained_layout=True)
    axs = fig.subplot_mosaic('EE;EE;AB')

    axs['E'].plot(top_validate_2d[:,0], top_validate_2d[:,1], 'ro', label='Top')
    axs['E'].plot(bad_validate_2d[:,0], bad_validate_2d[:,1], 'ko', label='Bad')
    axs['E'].set_title('All')
    axs['E'].legend(loc='upper left', fontsize=16)

    axs['A'].plot(bad_validate_2d[:,0], bad_validate_2d[:,1], 'ko', label='Bad')
    axs['A'].set_xlim(axs['E'].get_xlim())
    axs['A'].set_ylim(axs['E'].get_ylim())
    axs['A'].set_title('Bad')
    axs['B'].plot(top_validate_2d[:,0], top_validate_2d[:,1], 'ro', label='Top')
    axs['B'].set_xlim(axs['E'].get_xlim())
    axs['B'].set_ylim(axs['E'].get_ylim())
    axs['B'].set_title('Top')
    plt.savefig(path.join(save_path, output_file))



def log_plots(basedir, plot_directory):
    """
    Upload plots as artifacts to tracking server.

    Arguments
    ---------
    basedir : str
        Experiment base directory.
    plot_directory : str
        Plot files directory.
    """
    import mlflow
    from utils.tracking import Log as log
    from configparser import ConfigParser

    config = ConfigParser()
    config.read(path.join(basedir, 'mlflow-ids.ini'))
    ft_experiment_id = config.get('FastText', 'experiment_id')
    ft_run_id = config.get('FastText', 'run_id')

    tracking_url = environ.get('MLFLOW_TRACKING_URI')

    if tracking_url is not None:
        mlflow.set_tracking_uri(tracking_url)

    with mlflow.start_run(ft_run_id, ft_experiment_id):
        log.artifacts(plot_directory)


def get_latest_directory(path):
    """
    Returns the name of the latest (most recent) directory of the joined path(s).

    Arguments
    ---------
    path : str
        Directory path to examine.
    
    Returns
    -------
    str
        Directory path with latest created file inside.
    """
    directories = iglob(f"{path}/**/", recursive=True)
    if not directories:
        return None
    return max(directories, key=os.path.getctime)


def main(config):
    """
    Main entry for the analyse.

    Arguments
    ---------
    config : Namespace
        Run configuration.
        Converted argument strings as object with matching attributes.
    """
    random_state = get_seed()
    model, pca = None, None
    directory = None
    if config.automatic:
        directory = get_latest_directory('./out')
        print(">", directory)
        model, pca = load_models(path.join(directory, 'fasttext-model.bin'), path.join(directory, 'pca-model.pkl'))

        latest_config = load(open(path.join(directory, 'config.pkl'), 'rb'))
        print(latest_config)
        exit
    else:
        directory = path.dirname(config.model)
        model, pca = load_models(config.model, config.pca)
        latest_config = load(open(path.join(directory, 'config.pkl'), 'rb'))

    plot_directory = path.join(directory, 'plots')
    makedirs(plot_directory, exist_ok=True)

    if model is None:
        print('Wrong path for model')
        exit()

    train_df = prepare(path.join(directory, 'fasttext-train.txt'), model)
    train_X_domains = train_df['name']
    train_X, train_y = extract_samples_and_labels(train_df)

    pca_1d = None
    pca_2d = None

    if pca is None:
        pca = PCA(n_components=1, random_state=random_state)
        pca.fit(train_X, train_y)

    if pca.n_components == 1:
        pca_1d = pca
        pca_2d = PCA(n_components=2, random_state=random_state)
        pca_2d.fit(train_X, train_y)

    if pca.n_components == 2:
        pca_2d = pca
        pca_1d = PCA(n_components=1, random_state=random_state)
        pca_1d.fit(train_X, train_y)


    validate_df = prepare(path.join(directory, 'fasttext-validate.txt'), model)
    validate_X_domains = validate_df['name']
    validate_X, validate_y = extract_samples_and_labels(validate_df)

    top_validate_1d = pca_1d.transform(validate_X[validate_y == 0])
    bad_validate_1d = pca_1d.transform(validate_X[validate_y == 1])

    top_validate_2d = pca_2d.transform(validate_X[validate_y == 0])
    bad_validate_2d = pca_2d.transform(validate_X[validate_y == 1])


    savefig_1d_pca(
        top_validate_1d=top_validate_1d,
        bad_validate_1d=bad_validate_1d,
        save_path=plot_directory,
    )
    print(f"1D plots saved in: {path.join(plot_directory, 'pca-1d.png')}")

    savefig_2d_pca(
        top_validate_2d=top_validate_2d,
        bad_validate_2d=bad_validate_2d,
        save_path=plot_directory,
    )
    print(f"2D plots saved in: {path.join(plot_directory, 'pca-2d.png')}")

    top_validate_1d = np.array([ fasttext_predict(domain, model) for domain in validate_X_domains[validate_y == 0]])
    bad_validate_1d = np.array([ fasttext_predict(domain, model) for domain in validate_X_domains[validate_y == 1]])

    print(f"Fasttext Top as bad classified: {len(top_validate_1d[top_validate_1d < 0])}")
    print(f"Fasttext Top as good classified: {len(top_validate_1d[top_validate_1d > 0])}")
    print(f"Fasttext Bad as good classified: {len(bad_validate_1d[bad_validate_1d > 0])}")
    print(f"Fasttext Bad as bad classified: {len(bad_validate_1d[bad_validate_1d < 0])}")

    savefig_1d_pca(
        top_validate_1d=top_validate_1d,
        bad_validate_1d=bad_validate_1d,
        save_path=plot_directory,
        output_file='fasttext-1d.png',
    )
    print(f"Fasttext 1D plots saved in: {path.join(plot_directory, 'fasttext-1d.png')}")

    log_plots(directory, plot_directory)


if __name__ == '__main__':
    main(parseArgs())
