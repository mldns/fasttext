#!/bin/bash
# cspell:enableCompoundWords
#########################################################################################
# You can source this script snippet to start each script under the same circumstances. #
#########################################################################################

# If the current directory is the script directory, then change the current directory
# to the root directory of the project and make sure that the user is back in the
# original directory (script directory) when the script is finished.
# shellcheck disable=SC2064
trap "cd '$(pwd)'" EXIT
if [ "$(pwd)" == "$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)" ]; then
    cd ..
fi
