#!/usr/bin/env bash
# cspell:words xargs
# cspell:enableCompoundWords
############################################################
# Generate documentation file in orgmode.
############################################################
# Arguments:
# 1. directroy path to operate (optional)
#    [Default: last folder in ./out (creation date)]
############################################################
# shellcheck disable=SC1091
source "$(dirname "$0")/script_base.sh"
############################################################

if [ $# -gt 0 ]; then
    basename=$(basename "$1")
    dir="$1"
else
    # get last (creation date)
    basedir="./out"
    # https://stackoverflow.com/a/7448828
    path="$(find "$basedir" -name "*.bin" -type f -print0 | xargs -0 stat --format '%Y :%y -- %n' | sort -nr | cut -d: -f2- | head -n 1)"
    path="${path##*-- }"
    path="$(dirname "$path")"
    basedir="$(dirname "$path")"
    basename=$(basename "$path")
    dir="$basedir/$basename"
fi

name="$dir/README.org"
plots=$(ls "$dir"/plots/*.png)
echo "$name"

# write README.org
{
    # title
    echo "#+TITLE: $basename"
    echo

    # errors
    if [ -f "$dir/errors.log" ]; then
        echo "* Errors"
        echo "#+begin_src"
        cat "$dir/errors.log"
        echo "#+end_src"
        echo
    fi

    # plots
    for plot_path in $plots; do
        plot_basename=${plot_path##*/}
        if [[ "$plot_basename" == *2d*.png ]]; then
            plot_width=500
        else
            plot_width=1000
        fi

        echo "* $plot_basename"
        echo "#+ATTR_ORG: :width ${plot_width}px"
        echo "[[file:plots/$plot_basename]]"
        echo
    done

    # configuration
    echo "* Configuration"
    echo "#+begin_src python"
    cat "$dir/config.txt"
    echo # due to missing endline while cat
    echo "#+end_src"
    echo

    # configuration > fasttext
    echo "** FastText"
    echo "*** Arguments"
    echo "#+begin_src json"
    cat "$dir/fasttext-config.json"
    echo # due to missing endline while cat
    echo "#+end_src"
    echo

    echo "*** Test"
    echo "#+begin_src json"
    cat "$dir/fasttext-test.json"
    echo # due to missing endline while cat
    echo "#+end_src"
    echo
} > "$name"
