#!/usr/bin/env python
############################################################
# Plot metrics.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

import matplotlib.pyplot as plt
import numpy as np
import json
from os import path


def tf_metrics():
    """
    Provide tensorflow metrics.

    Returns
    -------
    array
        Metric array with the following columns:
        name, accuracy, top-recall, bad-recall, top-precision, bad-precision
    """
    return [
        # name, accuracy, top-recall, bad-recall, top-precision, bad-precision
        ["basic-model-tf", 0.83, 0.77, 0.88, 0.87, 0.80],
        ["bi-model-tf", 0.84, 0.79, 0.88, 0.87, 0.81],
        ["distilbert-model-tf", 0.88, 0.89, 0.87, 0.88, 0.89],
        ["fasttext-model-tf", 0.88, 0.82, 0.95, 0.94, 0.84],
        ["gru-model-tf", 0.87, 0.82, 0.92, 0.91, 0.84],
    ]


def load_ft_based_metrics(paths, surfix=''):
    """
    Load FastText metrics from test files.

    Arguments
    ---------
    path : array
        test file paths to load
    surfix : str, optional
        additional surfix for model name

    Returns
    -------
    array
        Metric array with the following columns:
        name, accuracy, top-recall, bad-recall, top-precision, bad-precision
    """
    for file_path in paths:
        data = json.load(open(file_path, "r"))
        modelname = path.basename(path.dirname(file_path))

        yield [
            modelname + surfix,
            data["accuracy"],
            data["__label__top"]["recall"],
            data["__label__bad"]["recall"],
            data["__label__top"]["precision"],
            data["__label__bad"]["precision"],
        ]


def ft_metrics():
    """
    Provide FastText without PCA metrics.

    Returns
    -------
    array
        Metric array with the following columns:
        name, accuracy, top-recall, bad-recall, top-precision, bad-precision
    """
    paths = [
        "./docs/models/5d/fasttext-test.json",
        "./docs/models/dga_cut_5d/fasttext-test.json",
        "./docs/models/5d_not_cleaned_whitelist/fasttext-test.json",
    ]
    return list(load_ft_based_metrics(paths, '_ft'))


def ft_pca_metrics():
    """
    Provide FastText with PCA metrics.

    Returns
    -------
    array
        Metric array with the following columns:
        name, accuracy, top-recall, bad-recall, top-precision, bad-precision
    """
    paths = [
        "./docs/models/5d/pca-test.json",
        "./docs/models/dga_cut_5d/pca-test.json",
        "./docs/models/5d_not_cleaned_whitelist/pca-test.json",
    ]
    return list(load_ft_based_metrics(paths, '_ft_pca'))


def bar_plot(
    title,
    x,
    y,
    xlim_min=0.7,
    xlim_max=1,
    reverse=False,
    figsize=(10,5),
    show=False,
    save=True
):
    """
    Create a metric bar plot.

    Arguments
    ---------
    title : str
        Plot title.
    x : array
        Labels for the metric values.
    y : array
        Metric values.
    xlim_min : float, optional
        X-axes minimum limitation.
    xlim_max : float, optional
        X-axes maximum limitation.
    reverse : bool, optional
        Reverse sort order.
    figsize : tuple, optional
        Figure size.
    show : bool, optional
        Show plot.
    save : bool, optional
        Save plot to file.
    """
    # force metric numbers to be numbers instead of strings
    y = np.array(y).astype(np.float64)

    # sort
    y, x = zip(*sorted(zip(y, x), reverse=reverse))

    # create figure
    fig = plt.figure(figsize=figsize)
    ax = plt.subplot(111)
    ax.barh(x, y, align="center")
    ax.set_xlim((xlim_min, xlim_max))
    plt.title(title)
    plt.tight_layout()

    # add bar labels with each metric number
    for i, v in enumerate(y):
        color = "black"
        pos = v
        if v - ((xlim_max - xlim_min) / 12.0) > xlim_min:
            pos = v - ((xlim_max - xlim_min) / 12.0)
            color = "white"
        ax.text(
            pos,
            i,
            " " + str(round(v, 3)),
            color=color,
            va="center",
            fontweight="bold",
        )

    if save:
        plt.savefig(title.replace(" ", "_") + ".png")
    if show:
        plt.show()


def main():
    """
    Main entry for the metric plot script.
    """
    tf_results = tf_metrics()
    ft_results = ft_metrics()
    ft_pca_results = ft_pca_metrics()

    # merge and sort results (metrics)
    results = tf_results + ft_results + ft_pca_results
    results = np.array(results)
    results = results[results[:, 1].argsort()]

    # plot metrics
    bar_plot("Accuracy", results[:, 0], results[:, 1])
    bar_plot("Top Recall", results[:, 0], results[:, 2])
    bar_plot("Bad Recall", results[:, 0], results[:, 3])
    bar_plot("Top Precision", results[:, 0], results[:, 4])
    bar_plot("Bad Precision", results[:, 0], results[:, 5])

    # plot training time
    bar_plot(
        "Training Time in Minutes",
        ["FastText", "Tensorflow", "Matlab"],
        [5, 5 * 60, 24 * 60],
        xlim_min=0,
        xlim_max=25 * 60,
        reverse=True
    )

    # plot model size
    names=["dga_cut_5d_ft", "5d_not_cleaned_ft", "5d_ft", "fasttext-model-tf", "distilbert-model-tf", "gru-model-tf", "bi-model-tf", "basic-model-tf"]
    memories = [202.7, 57.9,66.0, 82.0, 256.0, 311, 18, 4.1]
    bar_plot("Memory in MB", names, memories, xlim_max=320, xlim_min=0, reverse=True)


if __name__ == '__main__':
    main()
