#!/usr/bin/env python3
############################################################
# Prepare black and white lists.
############################################################
# Preparation tasks:
# - merge multiple lists
# - remove duplicates
# - save prepared data in {black,white}list.txt
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

import pandas as pd
import tarfile
import tempfile

from argparse import ArgumentParser
from utils.argparser_ext import directory

from urllib.request import Request, urlopen, urlretrieve
from csv import QUOTE_NONE as CSV_QUOTE_NONE


def prepare_df(df):
    """
    Prepare data frame.

    - drop missing values
    - drop duplicate values

    Arguments
    ---------
    df : pd.DataFrame
        data frame

    Returns
    -------
    pd.DataFrame
        Prepared data frame.
    """

    m_df = df
    m_df = m_df[['domain']]

    m_df = m_df.dropna()
    m_df = m_df.drop_duplicates('domain')

    return m_df


def prepare_whitelist():
    """
    Download, prepare and save whitelist.

    Arguments
    ---------
    whitelist_path : str
        file path to save whitelist

    Returns
    -------
    pd.DataFrame, str array
        Prepared whitelist data frame.
    """
    url = 'http://s3-us-west-1.amazonaws.com/umbrella-static/top-1m.csv.zip'
    top_df = pd.read_csv(url, header=None, names=['rank', 'domain'], usecols=[1])
    top_df = prepare_df(top_df)

    return top_df


def download_toulouse_list(category, archive_path=None):
    """
    Download a Université Toulouse 1 Capitole blacklist category.

    Arguments
    ---------
    category : str
        Blacklist category name.
    archive_path: str, optional
        Directory path to compressed csv file inside the category archive.
        Default is the category name itself.

    Returns
    -------
    pd.DataFrame
        Data frame from toulouse list for the given category.
    """
    url = f"http://dsi.ut-capitole.fr/blacklists/download/{category}.tar.gz"
    archive = f"{tempfile.gettempdir()}/{category}.tar.gz"
    urlretrieve(url, archive)
    csv_file = f"{category if archive_path is None else archive_path}/domains"
    with tarfile.open(archive, 'r') as tar:
        return pd.read_csv(
            tar.extractfile(csv_file),
            header=None,
            comment='#',
            names=['domain'],
            usecols=[0],
        )


def load_blacklists():
    """
    Download blacklists.

    Returns
    -------
    array of pd.DataFrame
        List of blacklist data frames.
    """
    bad_dfs = []

    # 1. blacklist: COVID-19 Cyber Threat Coalition
    # workaround since site does not accept Python requests, only Browser
    url = 'https://blocklist.cyberthreatcoalition.org/vetted/domain.txt'
    req = Request(url)
    req.add_header(
        'User-Agent',
        'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0',
    )
    content = urlopen(req)
    bad_dfs.append(
        pd.read_csv(content, header=None, comment='#', names=['domain'], delimiter=' ')
    )

    # 2. blacklist: StevenBlack
    url = 'https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts'
    bad_dfs.append(
        pd.read_csv(
            url,
            header=None,
            comment='#',
            names=['IP', 'domain'],
            delimiter=' ',
            usecols=[1],
            skiprows=30,
        )
    )

    # 3. blacklist: Université Toulouse
    categories = [
        ('malware', 'phishing'),
        ('phishing', 'phishing'),
    ]
    for category, archive_path in categories:
        bad_dfs.append(download_toulouse_list(category, archive_path))

    return bad_dfs


def prepare_blacklist():
    """
    Download, prepare and save blacklist.

    Returns
    -------
    pd.DataFrame
        Prepared blacklist data frame.
    """

    # download
    bad_dfs = load_blacklists()

    # concat and prepare
    bad_df = pd.concat(bad_dfs, ignore_index=True)
    bad_df = prepare_df(bad_df)

    return bad_df


def main(config):
    """
    Main entry for the prepare dataset script.

    Arguments
    ---------
    config : Namespace
        Run configuration.
        Converted argument strings as object with matching attributes.
    """
    top_df = prepare_whitelist()
    bad_df = prepare_blacklist()

    common = top_df['domain'].isin(bad_df['domain'])
    print(
        'Common in both top and bad:',
        len(top_df[common == True]),
        '(removed in top)'
    )
    top_df = top_df[common == False]

    print('top:', len(top_df))
    print('bad:', len(bad_df))

    # delete empty or only white spaces
    top_df = top_df[top_df['domain'].str.strip().astype(bool)]

    top_df['domain'].to_csv(
        path.join(config.out_dir, config.whitelist),
        index=False,
        header=False,
        quoting=CSV_QUOTE_NONE,
    )

    # delete empty or only white spaces
    bad_df = bad_df[bad_df['domain'].str.strip().astype(bool)]

    bad_df['domain'].to_csv(
        path.join(config.out_dir, config.blacklist),
        index=False,
        header=False,
        quoting=CSV_QUOTE_NONE,
    )


def parseArgs():
    """
    Parse command-line arguments.

    Returns
    -------
    Namespace
        Run configuration.
        Converted argument strings as object with matching attributes.
    """
    parser = ArgumentParser(description='{Black,White}list preparation')

    parser.add_argument(
        '--out-dir',
        type=directory(),
        default=path.abspath(path.join(path.dirname(__file__), '../datasets')),
        help='prepared dataset directory',
    )

    parser.add_argument(
        '--whitelist',
        type=str,
        default='whitelist.txt',
        help='whitelist name',
    )

    parser.add_argument(
        '--blacklist',
        type=str,
        default='blacklist.txt',
        help='blacklist name',
    )

    config = parser.parse_args()
    return config


if __name__ == '__main__':
    main(parseArgs())
