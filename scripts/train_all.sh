#!/usr/bin/env bash
# cspell:words wordNgrams ngrams mlflow
# cspell:enableCompoundWords
############################################################
# Train different models with different parameters.
# Then plot a few diagrams and create documentation.
############################################################
# shellcheck disable=SC1091
source "$(dirname "$0")/script_base.sh"
############################################################

create_model_plots_args=("--automatic")
write_org_file_args=()
max_loop_count=5

train() {
    out_path=""
    for (( i=0; i<$#; i++ )); do
        if [ "${*:i:1}" == "--out" ]; then
            out_path="${*:$((i+1)):1}"
            break;
        fi
    done
    echo "--------------------------------------------------------------------------------"
    echo "---------- TRAIN ----------"
    # three chances before skipping
    for (( i=1; ; i++ )); do
        rm -rf "$out_path"
        ./train.py "$@"

        if [ -f "${out_path}/done" ]; then
            # everything is fine ;)
            break
        fi

        # delete mlflow runs
        sed -nr 's|^run_id=(.+)$|\1|p' "${out_path}/mlflow-ids.ini" | while read -r line; do
            mlflow runs delete --run-id "$line"
        done

        # do not loop infinity times
        if [ "$i" == "$max_loop_count" ]; then
            return
        fi
    done
    echo "---------- PLOT ----------"
    ./scripts/create_model_plots.py "${create_model_plots_args[@]}"
    echo "---------- DOCUMENTATION ----------"
    ./scripts/write_org_file.sh "${write_org_file_args[@]}"
    echo "--------------------------------------------------------------------------------"
}

whitelists=("whitelist" "whitelist_cleaned")
data_selections=(
    "whitelist_blacklist"
    "whitelist_blacklist_unbalanced"
    "whitelist_blacklist_cutted"
    # "whitelist_blacklist_and_dga"
    # "whitelist_blacklist_and_dga_cutted"
)
seeds=(42 365 4711)

echo "whitelists:      ${whitelists[*]}"
echo "data_selections: ${data_selections[*]}"
echo "seeds:           ${seeds[*]}"
echo "This will approximate take $(echo "$(( 5 * 5 * ${#whitelists[*]} * ${#data_selections[*]} * ${#seeds[*]} )) * 2.5" | bc) minutes."
echo "================================================================================"

start=$(date +%s.%N)

for seed in ${seeds[*]}; do
    for data_selection in ${data_selections[*]}; do
        create_model_plots_args=("--automatic")

        for whitelist in ${whitelists[*]}; do
            # 5D
            train \
                --data-selection "$data_selection" \
                --whitelist_path "./datasets/${whitelist}.txt" \
                --fasttext dim=5 \
                --seed="$seed" \
                --out "./out/${seed}/${whitelist}/${data_selection}/5d"

            # 5D + www-cut + without-tld
            train \
                --data-preprocessing trim_leading_www remove_tld lower_case_split_by_dot_dash_underscore \
                --data-selection "$data_selection" \
                --whitelist_path "./datasets/${whitelist}.txt" \
                --fasttext dim=5 \
                --seed="$seed" \
                --out "./out/${seed}/${whitelist}/${data_selection}/www_tld_5d"

            # 20D + www-cut + without-tld + word-length-5
            train \
                --data-preprocessing trim_leading_www remove_tld lower_case_split_by_dot_dash_underscore max_word_length_5 \
                --data-selection "$data_selection" \
                --whitelist_path "./datasets/${whitelist}.txt" \
                --fasttext dim=20 wordNgrams=2 \
                --seed="$seed" \
                --out "./out/${seed}/${whitelist}/${data_selection}/www_tld_l5_20d_N2"

            # 5D + ngrams-3 + www-cut + without-tld + word-length-3
            train \
                --data-preprocessing trim_leading_www remove_tld lower_case_split_by_dot_dash_underscore max_word_length_3 \
                --data-selection "$data_selection" \
                --whitelist_path "./datasets/${whitelist}.txt" \
                --fasttext dim=5 wordNgrams=3 \
                --seed="$seed" \
                --out "./out/${seed}/${whitelist}/${data_selection}/www_tld_l3_dga_5d_N3"

            # 10D + autotuneMetric-r2p-96-top
            train \
                --data-selection "$data_selection" \
                --whitelist_path "./datasets/${whitelist}.txt" \
                --fasttext dim=10 autotuneMetric=recallAtPrecision:96:__label__top \
                --seed="$seed" \
                --out "./out/${seed}/${whitelist}/${data_selection}/10d_RP96top"

        done
    done
done

end=$(date +%s.%N)
echo "$end - $start" | bc -l
