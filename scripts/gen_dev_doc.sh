#!/bin/sh
# cspell:words pdoc xelatex
# cspell:enableCompoundWords
############################################################
# Generate developer documentation.
############################################################
# Generate html, md and pdf.
############################################################

# pdoc exclude paths
# Note: Only project root elements are supported!
pdoc_exclude="setup.py out docs datasets"
# create temporary dictionary
tmp=$(mktemp -d)

# cleanup by move everthing back
cleanup() {
    echo "[info] move excluded files back"
    for path in ${pdoc_exclude}; do
        mv "$tmp/$path" ./
    done
    rm -r "$tmp"
    echo "[info] END"
}
trap cleanup EXIT

echo "[info] START"

# exclude by moving into another directory
echo "[info] move exclude files"
for path in ${pdoc_exclude}; do
    mv "./$path" "$tmp"
done

# create documentation
echo "[info] generate html documentation"
pdoc --html ./ --force -o "$tmp/docs/developer/html"

echo "[info] generate markdown (required for pdf) documentation"
pdoc --pdf ./ 2>/dev/null > "$tmp/docs/developer/documentation.md"

echo "[info] generate pdf documentation"
pandoc --metadata-file "$tmp/docs/developer/config.yaml"   \
    --from=markdown+tex_math_single_backslash              \
    --pdf-engine=xelatex                                   \
    --variable=mainfont:"DejaVu Sans"                      \
    --toc --toc-depth=4                                    \
    --output="$tmp/docs/developer/documentation.pdf"       \
    "$tmp/docs/developer/documentation.md"
