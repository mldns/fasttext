############################################################
# Collection of functions that improve the handling with
# file and directories.
############################################################

from enum import IntEnum
from os import path


class SIZE_UNIT(IntEnum):
    """
    Size Units like MB and GB.
    """
    BYTES = 0
    KB = 1
    MB = 2
    GB = 3
    TB = 4

    def __str__(self):
        """
        Get size unit representation.
        The size unit name is set as representation.

        Returns
        -------
        str
            Size unit string representation.
        """
        return self.name


def convert_unit(size, from_unit=SIZE_UNIT.BYTES, to_unit=None):
    """
    Convert the size from one unit to another like KB → MB.

    Arguments
    ---------
    size : int, float, number
        Size which should be converted.
    from_unit : SIZE_UNIT, optional
        Source size unit. Unit of the size argument.
        Bytes is assumed by default.
    to_unit : SIZE_UNIT, optional
        Target size unit. Unit the size argument should be converted to.
        By default the biggest possible will be used.

    Returns
    -------
    float [, size_unit]
        Converted size and size unit if target size unit is not specific set.
    """
    if to_unit is not None:
        # the nifty part is, that downsizing like MB → KB is also possible here
        return size / (1024**(to_unit-from_unit))

    unit = from_unit
    while (size // 1024) > 0:
        size = size / 1024
        unit = unit + 1
    return size, unit


def get_file_size(file_path, size_unit=None):
    """
    Get file in size in given unit like KB, MB or GB.

    Arguments
    ---------
    file_path : str
        Path to file which should be inspect.
    size_unit : SIZE_UNIT, optional
        Size unit like MB or GB.
        By default the biggest possible will be used.

    Returns
    -------
    float [, size_unit]
        File size and size unit if not specific set.
    """
    size = path.getsize(file_path)
    return convert_unit(size, to_unit=size_unit)
    