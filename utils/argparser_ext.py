############################################################
# Collection of type functions to extend pythons
# argument parser library argparse.
############################################################

from argparse import Action, ArgumentError, ArgumentTypeError
from os import path
import inspect


def intOrStr(v):
    """
    Convert command-line argument value to integer if possible.

    Arguments
    ---------
    v : str
        command-line argument value

    Returns
    -------
    int or str
        value as integer if possible; otherwise value as string
    """
    if v.isdigit():
        return int(v)
    return v


def str2bool(v):
    """
    Convert command-line argument value to boolean.

    Arguments
    ---------
    v : str
        command-line argument value

    Returns
    -------
    bool
        value as boolean

    Source
    ------
        https://stackoverflow.com/a/43357954
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    raise ArgumentTypeError('Boolean value expected.')


def fromEnum(T):
    """
    Convert command-line argument value to specific Enum.

    Arguments
    ---------
    T : class type
        Enum type

    Returns
    -------
    function
        type function which converts command-line value to Enum T
    """
    def test(v):
        """
        Convert command-line argument value to Enum T.

        Arguments
        ---------
        v : str
            command-line argument value

        Returns
        -------
        T
            value as Enum T instance
        """
        # look for enum key (case insensitive)
        for e in T:
            if str(v).upper() == e.name.upper():
                return e

        # look for enum value
        for e in T:
            try:
                v2 = type(e.value)(v)
                if v2 == e.value:
                    return e
            except Exception:
                pass

        # neither enum key not value found
        raise ArgumentError()

    return test


def file(v):
    """
    Check if command-line argument value is existing file path.

    Arguments
    ---------
    v : str
        command-line argument value

    Returns
    -------
    str
        file path with expanded home directory if used
    """
    f = path.expanduser(v)
    if path.isfile(f):
        return f
    raise ArgumentTypeError('Existing file expected.')


def directory(exist=True, expanduser=True, expandvars=True, abspath=False):
    """
    Check if directory path is valid.

    Arguments
    ---------
    exists : bool, optional
        directory path needs to exists
    expanduser : bool, optional
        use absulute home path instead of tilde
    expandvars : bool, optional
        use absulute path instead of path variable
    abspath : bool, optional
        use absulute path instead of relative path

    Returns
    -------
    function
        type function which evaluates command-line value
    """
    def test(v):
        """
        Check and adjust directory path.

        Arguments
        ---------
        v : str
            command-line argument value

        Returns
        -------
        str
            adjusted and valid directory path
        """
        d = v
        if expanduser:
            d = path.expanduser(d)
        if expandvars:
            d = path.expandvars(d)
        if abspath:
            d = path.abspath(d)

        if exist:
            if path.isdir(d):
                return d
        else:
            if not path.exists(d) or path.isdir(d):
                return d

        raise ArgumentTypeError('Existing directory expected')

    return test


def cls_method(cls):
    """
    Check if command-line argument value is an existing class function.

    Arguments
    ---------
    cls : class type
        Class where the chosen function is located.

    Returns
    -------
    function
        type function which evaluates command-line value
    """
    def methods():
        """
        Get all class functions.

        Returns
        -------
        array of (str, function) tuples
            list of class function name and class function tuples
        """
        for method, fn in inspect.getmembers(cls, predicate=inspect.isfunction):
            if not method.startswith('_'):
                yield method, fn

    def test(v):
        """
        Convert command-line argument value to class function pointer.

        Arguments
        ---------
        v : str
            command-line argument value

        Returns
        -------
        function
            class function
        """
        for method, fn in methods():
            if method == v:
                return fn

        raise ArgumentTypeError(
            f"Unknow method: '{v}'\nAvailible: " + \
            ', '.join([method for method, _ in methods()])
        )

    return test


class StoreDictKeyPair(Action):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __call__(self, parser, namespace, values, option_string):
        dest = {}
        for key_value_pair in values:
            key, value = key_value_pair.split('=')

            if value.isdigit():
                value = int(value)
            elif value.replace('.','',1).isdigit():
                value = float(value)

            dest[key] = value
        setattr(namespace, self.dest, dest)
