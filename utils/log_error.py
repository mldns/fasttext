############################################################
# Collection of error helper functions.
############################################################

from sys import stderr
from os import path

from utils.tracking import Log as log


ERROR_FILE_NAME='errors.log'


def get_error_file_path(output_file_base_path):
    """
    Get error file path.

    Arguments
    ---------
    output_file_base_path : str
        Directory path with file prefix to save files like model exports.

    Returns
    -------
    str
        Error file path.
    """
    return path.join(output_file_base_path[:(output_file_base_path.rfind('/'))], ERROR_FILE_NAME)


def log_error(msg, output_file_base_path):
    """
    Log error into error file and stderr.

    Arguments
    ---------
    msg : str
        Error log message.
    output_file_base_path : str
        Directory path with file prefix to save files like model exports.
    """
    # log to stderr
    print(msg, file=stderr)

    # log to error file
    file = get_error_file_path(output_file_base_path)
    with open(file, 'a') as f:
        f.write(f"{msg}\n")


def upload_errors(output_file_base_path):
    """
    Upload error file if exists.

    Arguments
    ---------
    output_file_base_path : str
        Directory path with file prefix to save files like model exports.
    """
    if path.exists(ERROR_FILE_NAME):
        file = get_error_file_path(output_file_base_path)
        log.artifact(file)
