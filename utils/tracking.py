############################################################
# Tracking methods.
############################################################

import mlflow
from typing import Any, Dict, Optional


class Log():
    """
    Tracking Server Log Class.
    """

    def artifact(local_path: str, artifact_path: Optional[str] = None) -> None:
        """
        Log a local file or directory as an artifact of the currently active run.
        If no run is active, this method will create a new active run.

        Arguments
        ---------
        local_path : str
            Path to the file to write.
        artifact_path : str, optional
            If provided, the directory in `artifact_uri` to write to.

        Examples
        --------
        ```python
        import mlflow

        # Create a features.txt artifact file
        features = "rooms, zipcode, median_price, school_rating, transport"
        with open("features.txt", 'w') as f:
            f.write(features)

        # With artifact_path=None write features.txt under
        # root artifact_uri/artifacts directory
        with mlflow.start_run():
            mlflow.log_artifact("features.txt")
        ```
        """
        mlflow.log_artifact(local_path, artifact_path)

    def artifacts(local_dir: str, artifact_path: Optional[str] = None) -> None:
        """
        Log all the contents of a local directory as artifacts of the run.
        If no run is active, this method will create a new active run.

        Arguments
        ---------
        local_dir: str
            Path to the directory of files to write.
        artifact_path: str
            If provided, the directory in `artifact_uri` to write to.

        Examples
        --------
        ```python
        import os
        import mlflow

        # Create some files to preserve as artifacts
        features = "rooms, zipcode, median_price, school_rating, transport"
        data = {"state": "TX", "Available": 25, "Type": "Detached"}

        # Create couple of artifact files under the directory "data"
        os.makedirs("data", exist_ok=True)
        with open("data/data.json", 'w', encoding='utf-8') as f:
            json.dump(data, f, indent=2)
        with open("data/features.txt", 'w') as f:
            f.write(features)

        # Write all files in "data" to root artifact_uri/states
        with mlflow.start_run():
            mlflow.log_artifacts("data", artifact_path="states")
        ```
        """
        mlflow.log_artifact(local_dir, artifact_path)

    def param(key: str, value: Any) -> None:
        """
        Log a parameter under the current run.
        If no run is active, this method will create a new active run.

        Arguments
        ---------
        key : str
            Parameter name.
        value : any
            Parameter value (string, but will be stringified if not).

        Examples
        --------
        ```python
        import mlflow

        with mlflow.start_run():
            mlflow.log_param("learning_rate", 0.01)
        ```
        """
        mlflow.log_param(key, value)

    def params(params: Dict[str, Any]) -> None:
        """
        Log a batch of params for the current run.
        If no run is active, this method will create a new active run.

        Arguments
        ---------
        params: dict
            Dictionary of param_name (str) -> value (string, but will be stringified if not).

        Examples
        --------
        ```python
        import mlflow

        params = {"learning_rate": 0.01, "n_estimators": 10}

        # Log a batch of parameters
        with mlflow.start_run():
            mlflow.log_params(params)
        ```
        """
        mlflow.log_params(params)

    def metric(key: str, value: float, step: Optional[int] = None) -> None:
        """
        Log a metric under the current run.
        If no run is active, this method will create a new active run.

        Arguments
        ---------
        key: str
            Metric name.
        value: float
            Metric value.
            Note that some special values such as +/- Infinity may be replaced by other
            values depending on the store.
            For example, the SQLAlchemy store replaces +/- Infinity with max / min float values.
        step: int, optional
            Metric step. Defaults to zero if unspecified.

        Examples
        --------
        ```python
        import mlflow

        with mlflow.start_run():
            mlflow.log_metric("mse", 2500.00)
        ```
        """
        mlflow.log_metric(key, value, step)

    def metrics(metrics: Dict[str, float], step: Optional[int] = None) -> None:
        """
        Log multiple metrics for the current run.
        If no run is active, this method will create a new active run.

        Arguments
        ---------
        metrics: dict
            Dictionary of metric_name (str) -> value (float).
            Note that some special values such as +/- Infinity may be replaced by other
            values depending on the store.
            For example, sql based store may replace +/- Infinity with max / min float values.
        step: int, optional
            A single integer step at which to log the specified Metrics.
            If unspecified, each metric is logged at step zero.

        Examples
        --------
        ```python
        import mlflow

        metrics = {"mse": 2500.00, "rmse": 50.00}

        # Log a batch of metrics
        with mlflow.start_run():
            mlflow.log_metrics(metrics)
        ```
        """
        mlflow.log_metrics(metrics, step)
