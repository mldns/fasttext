#!/usr/bin/env python3
############################################################
# Train models.
############################################################
# Steps:
#   0. Data Preprocessing (split/manipulate domain names)
#   1. FastText Model (word vectors)
#   2. Word Vector Scaling
#        (standardization, normalization, ...)
#   3. PCA (dimension reduction)
#   4. further models (SVM)
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.dirname(__file__))
############################################################

from utils.tracking import Log as log
import mlflow
from utils.log_error import upload_errors
from settings import set_seed

from args import parseArgs
from os import makedirs, environ, path
from pathlib import Path
from pickle import dump

from models.fasttext_model import FastTextModel
from models.pca_model import PCAModel
from models.svm_model import SVMModel

from executor import Executor


def log_mlflow_ids(file, section, experiment_id, run_id):
    """
    Save mlflow IDs inside an ini formatted file.

    Arguments
    ---------
    file : str
        Path to file which should be appended.
    section : str
        Ini section name.
    experiment_id : int
        MlFlow experiment id.
    run_id : int
        MlFlow run id.
    """
    with open(file, 'a') as f:
        f.write(f"[{section}]\n")
        f.write(f"experiment_id={experiment_id}\n")
        f.write(f"run_id={run_id}\n")


def main(config):
    """
    Main entry for the training script.

    Arguments
    ---------
    config : Namespace
        Run configuration.
        Converted argument strings as object with matching attributes.
    """
    # print run configurations
    print(config)
    run_name = path.basename(config.out)

    # set seed
    if config.seed is not None:
        set_seed(config.seed)

    # make output dir
    makedirs(config.out)

    # save run configurations
    with open(f"{config.output_file_base_path}config.txt", 'w') as outfile:
        outfile.write(str(config))
    dump(config, open(f"{config.output_file_base_path}config.pkl", 'wb'))

    # get/select data
    X_train, y_train, X_validate, y_validate, X_test, y_test = config.data_selection(
        allow_csv=config.whitelist_path,
        deny_csv=config.blacklist_path,
        dga_csv=config.dga_path,
    )

    # initialize executor
    executor = Executor(
        X_train, y_train, X_validate, y_validate, X_test, y_test,
        config.data_preprocessing,
        config.output_file_base_path,
        early_stopping=False,
    )
    #executor.print_validation()

    # FastText
    ft_experiment_id = 0
    ft_run_id = 0
    if config.fasttext != None:
        print('')

        mlflow.set_experiment('FastText')
        with mlflow.start_run(run_name=run_name) as run:
            ft_experiment_id = run.info.experiment_id
            ft_run_id = run.info.run_id
            log_mlflow_ids(f"{config.output_file_base_path}mlflow-ids.ini", 'FastText', ft_experiment_id, ft_run_id)

            log.param('seed', config.seed)
            log.artifact(f"{config.output_file_base_path}config.txt")
            log.artifact(f"{config.output_file_base_path}config.pkl")

            executor.do_model(
                FastTextModel,
                config.fasttext_model,
                config.fasttext,
                scaling=config.fasttext_word_vector_scaling,
                threshold=config.fasttext_threshold,
            )
            #executor.print_validation()
            upload_errors(config.output_file_base_path)


    # PCA
    pca_experiment_id = 0
    pca_run_id = 0
    if config.pca != None:
        print('')

        mlflow.set_experiment('PCA')
        mlflow.sklearn.autolog()
        with mlflow.start_run(run_name=run_name) as run:
            pca_experiment_id = run.info.experiment_id
            pca_run_id = run.info.run_id
            log_mlflow_ids(f"{config.output_file_base_path}mlflow-ids.ini", 'PCA', pca_experiment_id, pca_run_id)

            log.param('FastText Experiment ID', ft_experiment_id)
            log.param('FastText Run ID', ft_run_id)
            log.param('seed', config.seed)

            executor.do_model(
                PCAModel,
                config.pca_model,
                config.pca,
                scaling=config.pca_output_scaling,
                threshold=config.pca_threshold,
            )
            #executor.print_validation()
            upload_errors(config.output_file_base_path)

    # SVM
    svm_experiment_id = 0
    svm_run_id = 0
    if config.svm != None:
        print('')

        mlflow.set_experiment('SVM')
        mlflow.sklearn.autolog()
        with mlflow.start_run(run_name=run_name) as run:
            svm_experiment_id = run.info.experiment_id
            svm_run_id = run.info.run_id
            log_mlflow_ids(f"{config.output_file_base_path}mlflow-ids.ini", 'SVM', svm_experiment_id, svm_run_id)

            log.param('FastText Experiment ID', ft_experiment_id)
            log.param('FastText Run ID', ft_run_id)
            log.param('PCA Experiment ID', pca_experiment_id)
            log.param('PCA Run ID', pca_run_id)
            log.param('seed', config.seed)

            executor.do_model(
                SVMModel,
                config.svm_model,
                config.svm,
                threshold=config.svm_threshold,
            )
            #executor.print_validation()
            upload_errors(config.output_file_base_path)

    # Prediction: google.com
    print('')
    print('predict: th-koeln.de')
    domain = 'th-koeln.de'
    y = executor.predict(domain)
    print(f"  {y}")

    Path(path.join(config.out, 'done')).touch(exist_ok=True)


if __name__ == '__main__':
    tracking_url = environ.get('MLFLOW_TRACKING_URI')
    if tracking_url is not None:
        mlflow.set_tracking_uri(tracking_url)

    main(parseArgs())
