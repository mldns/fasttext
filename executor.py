############################################################
# Trainings Executor
############################################################
# Execute a single step.
############################################################

from data.scaling import DataScaling
from utils.log_error import log_error


class Executor:
    """
    Trainings Executor to help you with all the complex stuff.
    """

    def __init__(
        self,
        X_train,
        y_train,
        X_validate,
        y_validate,
        X_test,
        y_test,
        data_preprocessing,
        output_file_base_path,
        early_stopping=True,
    ):
        """
        Initialize trainings executor instance.

        Arguments
        ---------
        X_train : array, np.ndarray or ndarray-like
            List of training domains.
        y_train : array, np.ndarray or ndarray-like
            List of training labels (according to the list of training domains).
        X_validate : array, np.ndarray or ndarray-like
            List of validation domains.
        y_validate : array, np.ndarray or ndarray-like
            List of validation labels (according to the list of validation domains).
        X_test : np.ndarray
            List of testing domains.
        y_test : np.ndarray
            List of testing labels (according to the list of testing domains).
        data_preprocessing : array of function pointers
            List of data preprocessing steps.
        output_file_base_path : str
            Directory path with possible file prefix to save files like model exports.
        early_stopping : bool, optional
            Early stopping activation state.
        """
        self.X_train = X_train
        self.y_train = y_train
        self.X_validate = X_validate
        self.y_validate = y_validate
        self.X_test = X_test
        self.y_test = y_test

        self.data_preprocessing = data_preprocessing
        self.output_file_base_path = output_file_base_path

        self.early_stopping = early_stopping

        # model stack
        self.models = []
        # next transformation: next transformation if an additional model is used
        self.next_transformation = None

        # run pipeline once
        self._run_pipeline_internal()

    def predict(self, domain):
        """
        Predict single domain name.

        Arguments
        ---------
        domain : str
            Domain name.

        Returns
        -------
        str
            Predicted label.
        """
        [[x]] = Executor.run_pipeline(self.data_preprocessing, [domain])
        return self.models[-1].predict(x)

    def run_pipeline(pipeline, *arrays):
        """
        Apply pipeline to list of items (domain names).

        Arguments
        ---------
        pipeline : array of function
            Functions to apply on each list item (domain name).
        *arrays : array
            One or more lists of items (domain names) which should run through the pipeline.

        Returns
        -------
        array
            Modified lists.
        """
        result = []
        for domains in arrays:
            for fn in pipeline:
                domains = [fn(x) for x in domains]
            result.append(domains)
        return result

    def _run_pipeline_internal(self, fn=None):
        """
        Apply data preprocessing pipeline or specific function to the training,
        validation and testing datasets (domain names).

        Arguments
        ---------
        fn : function, optional
            Use specific function pointer instead of data preprocessing pipeline.
        """
        pipeline = self.data_preprocessing if fn is None else [fn]
        self.X_train, self.X_validate, self.X_test = Executor.run_pipeline(
            pipeline,
            self.X_train,
            self.X_validate,
            self.X_test
        )

    def print_validation(self, n=10):
        """
        Print a few validation data.

        Arguments
        ---------
        n : int, optional
            Number of rows to print.
        """
        print(self.X_validate[:n])

    def do_model(self, Model, model_path=None, model_params={}, scaling=DataScaling.no_scaling, threshold=None):
        """
        Train a model, add necessary transformations to pipeline and do all to complex stuff for me.

        Arguments
        ---------
        Model : BaseModel
            Model class to train.
        model_path : str, optional
            Instead of training, load a model given a filepath.
        model_params : dict, optional
            Further model arguments which should be passed through.
        scaling : TransformerMixin, optional
            Data scaling / data transformation instance.
        threshold : float, optional
            Threshold between [-1, 1] for further prediction manipulation.
                -1 : nothing will every be evaluated as bad
                1 : only 100% sure pages will be evaluated as top
                0 : use model prediction default. Same as None only slower!
                None : same as 0. Only faster!
        """
        def transform(fn):
            self._run_pipeline_internal(fn)
            self.data_preprocessing.append(fn)

            # sanity check
            if sum([abs(vector_comp) for vector in self.X_validate[:10] for vector_comp in vector]) == 0:
                log_error(
                    'ERROR: All word vectors of the first 10 domains (X_validate) are all zero!',
                    self.output_file_base_path,
                )
                self.print_validation(10)
                if self.early_stopping:
                    exit(-1)

        if self.next_transformation != None:
            transform(self.next_transformation)
            self.next_transformation = None  # reset

        model = Model(self.output_file_base_path)
        if model_path == None:
            model.train(self.X_train, self.y_train, self.X_validate, self.y_validate, model_params)
            model.test(self.X_test, self.y_test, threshold=threshold, early_stopping=self.early_stopping)
        else:
            model.load(model_path)
            model.test(self.X_test, self.y_test, threshold=threshold, early_stopping=False)

        if model.transformation_necessary:
            transform(model.transform)  # data manipulation
        else:
            self.next_transformation = model.transform

        # optional: scaling
        if scaling != DataScaling.no_scaling:
            self.X_train, self.X_validate, self.X_test, scaler = scaling(self.X_train, self.X_validate, self.X_test)
            self.data_preprocessing.append(scaler.transform)

        # add model to stack
        self.models.append(model)
