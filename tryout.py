#!/usr/bin/env python3
############################################################
# Try out your models.
############################################################
# Steps:
#   1. choose model to test
#   2. input different domains and see their prediction
#      result
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.dirname(__file__))
############################################################

from os import path
import readline
from glob import glob

import fasttext
from sklearn.decomposition import PCA
from pickle import load

from settings import label
from executor import Executor


def load_models(model_path, pca_path):
    """
    Loads the Fasttext and PCA model.

    Arguments
    ---------
    model_path : str
        Path to Fasttext model (.bin)
    pca_path : str
        Path to PCA model (.pkl)

    Returns
    -------
    tuple
        Model and PCA
    """
    model = fasttext.load_model(model_path)
    pca = None
    if path.exists(pca_path):
        pca = load(open(pca_path, 'rb'))
    return model, pca


def prepare_domain(domain, config):
    """
    Preprocessed the domain by using config.

    The config is the config which was used on the creation of the model.
    So, the same preprocessing steps are performed here.

    Arguments
    ---------
    domain : str
        Domain name.
    config : dict
        Model training config.

    Returns
    -------
    any
        Preprocessed/Prepared domain
    """
    [[x]] = Executor.run_pipeline(config.data_preprocessing, [domain])
    return x


class TabCompleter(object):
    """
    A tab completer that can either complete from
    the filesystem or from a list.

    Partially taken from:
    http://stackoverflow.com/questions/5637124/tab-completion-in-pythons-raw-input

    Source:
    https://gist.github.com/iamatypeofwalrus/5637895
    """

    def pathCompleter(self, text, state):
        """
        This is the tab completer for systems paths.
        Only tested on *nix systems
        """
        line = readline.get_line_buffer().split()

        # replace ~ with the user's home dir. See https://docs.python.org/2/library/os.path.html
        text = path.expanduser(text)

        # autocomplete directories with having a trailing slash
        if path.isdir(text):
            text += '/'

        return [x for x in glob(text + "*")][state]


    def createListCompleter(self,ll):
        """
        This is a closure that creates a method that autocompletes from
        the given list.

        Since the autocomplete function can't be given a list to complete from
        a closure is used to create the listCompleter function with a list to complete
        from.
        """
        def listCompleter(text,state):
            line   = readline.get_line_buffer()

            if not line:
                return [c + " " for c in ll][state]

            else:
                return [c + " " for c in ll if c.startswith(line)][state]

        self.listCompleter = listCompleter


def main():
    """
    Main entry for the manual model testing script.
    """
    readline.set_completer_delims('\t')
    readline.parse_and_bind("tab: complete")

    t = TabCompleter()
    readline.set_completer(t.pathCompleter)

    model_path = input("FastText model (*.bin): ")
    pca_path = input("PCA model is exists (*.pkl): ")
    print()

    # load models and config
    model, pca = load_models(model_path, pca_path)
    config = load(open(path.join(path.dirname(model_path), 'config.pkl'), 'rb'))

    readline.parse_and_bind("tab: '    '")
    while True:
        domain = input("Domain to predict: ")
        domain = domain.strip()
        if domain == "":
            break

        domain = prepare_domain(domain, config)

        (predicted_label,), [probability] = model.predict(domain, k=1)
        print(f"==>  {predicted_label} (probability: {probability})")

if __name__ == "__main__":
    main()
