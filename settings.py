############################################################
# Default Settings.
############################################################

import numpy as np
from os import environ
import random


# Dataset labels
label = {
    'top': '__label__top',
    'bad': '__label__bad',
}


def set_seed(seed):
    """
    Set initialize internal state.

    Arguments
    ---------
    seed : int
        Initialize internal state value.
        If `None` initial internal state is random.
    """
    environ['PYTHONHASHSEED'] = str(seed)
    random.seed(seed)
    np.random.seed(seed)


def get_seed():
    """
    Get initialize internal state.

    Returns
    -------
    int
        Initialize internal state value.
        `None` if initial internal state should be randomly set.
    """
    seed = environ.get('PYTHONHASHSEED')
    if not seed:
        return None
    return int(seed)
