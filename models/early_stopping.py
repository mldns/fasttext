############################################################
# Early Stopping Evaluation.
############################################################

from settings import label


class EarlyStoppingBase():
    """
    Base Class for all early stopping evaluations.
    """

    def evaluate(self, result):
        """
        Evaluate if any critical thresholds are not fulfilled.

        Arguments
        ---------
        result : dict
            Test results from sklearn.metrics.classification_report as dict.

        Returns
        -------
        bool
            True if not all critical threshold are fulfilled (early stopping); otherwise False.
        """
        raise NotImplementedError("Subclasses should implement this!")


class RecallAccuracyEarlyStopping(EarlyStoppingBase):
    """
    Base class for all early stopping evaluations which are based on recall and accuracy.
    """

    def __init__(self, top_recall, bad_recall, accuracy):
        """
        Initialize early stopping instance based on recall and accuracy.

        Arguments
        ---------
        top_recall : float
            Critical threshold between [0, 1] for the top recall value which must be reached at least.
        bad_recall : float
            Critical threshold between [0, 1] for the bad recall value which must be reached at least.
        accuracy : float
            Critical threshold between [0, 1] for the accuracy value which must be reached at least.
        """
        self.top_recall = top_recall
        self.bad_recall = bad_recall
        self.accuracy = accuracy

    def evaluate(self, result):
        """
        Evaluate if any critical thresholds are not fulfilled.

        Arguments
        ---------
        result : dict
            Test results from sklearn.metrics.classification_report as dict.

        Returns
        -------
        bool
            True if not all critical threshold are fulfilled (early stopping); otherwise False.
        """
        return not (
            result[label['top']]['recall'] >= self.top_recall and \
            result[label['bad']]['recall'] >= self.bad_recall and \
            result['accuracy'] >= self.accuracy
        )


class FastTextEarlyStopping(RecallAccuracyEarlyStopping):
    """
    FastText early stopping class with special values which the FastText model should fulfilled at least.
    """

    def __init__(self):
        """
        Initialize early stopping instance based on recall and accuracy
        with special values which the FastText model should fulfilled at least.
        """
        super().__init__(0.88, 0.82, .8)
