############################################################
# Support Vector Machine (SVM) operations.
############################################################

from sklearn.svm import SVC
from pickle import dump, load
import json

from settings import label, get_seed
from utils.file import get_file_size, SIZE_UNIT
from utils.tracking import Log as log
from models.base_model import BaseModel


class SVMModel(BaseModel):
    """
    Support Vector Machine (SVM) model to handle all SVM specific operations.
    """

    def __init__(self, output_file_base_path, output_file_base_path_surfix='svm', early_stopper=None):
        """
        Initialize Support Vector Machine (SVM) model.

        Arguments
        ---------
        output_file_base_path : str
            Directory path with possible file prefix to save files like model exports.
        output_file_base_path_surfix : str, optional
            Special file prefix additional to output_file_base_path.
        early_stopper : EarlyStoppingBase, optional
            Early stopping handler.
        """
        super().__init__(
            output_file_base_path + output_file_base_path_surfix,
            early_stopper,
        )
        self.transformation_necessary = False

    def transform(self, domain_vector):
        """
        Apply model transformations on a single domain vector representation.
        WARNING: SMV is not further stackable. SVM should be the last model.
                 This method won't do anything.

        Arguments
        ---------
        domain_vector : float array
            Domain vector representation.

        Returns
        -------
        float array
            Domain vector representation (unchanged).
        """
        print('WARN:  SMV is not further stackable. SVM should be the last model.')
        return domain_vector

    def load(self, model_path=None):
        """
        Load a model given a filepath and return a model object.

        Arguments
        ---------
        model_path : str
            File path to exported model to load.

        Returns
        -------
        sklearn.svm.SVC
            SVM model.
        """
        if model_path == None:
            model_path = f"{self.output_file_base_path}-model.pkl"

        self.model = load(open(model_path, 'rb'))

        return self

    def train(self, X_train, y_train, X_validate, y_validate, train_params):
        """
        Train the model and export the result.

        Arguments
        ---------
        X_train : array, np.ndarray or ndarray-like
            List of training domains.
        y_train : array, np.ndarray or ndarray-like
            List of training labels (according to the list of training domains).
        X_validate : array, np.ndarray or ndarray-like
            List of validation domains.
        y_validate : array, np.ndarray or ndarray-like
            List of validation labels (according to the list of validation domains).
        train_params : dict, optional
            Further SVC initialize arguments.

        Returns
        -------
        SVMModel
            Self.
        """
        random_state = get_seed()
        self.model = SVC(
            random_state=random_state,
            **train_params,
        )
        self.model.fit(X_train, y_train)

        # save training parameters
        with open(f"{self.output_file_base_path}-config.json", 'w') as outfile:
            json.dump(train_params, outfile, indent=4, sort_keys=True)
        log.artifact(f"{self.output_file_base_path}-config.json")

        # save model
        dump(self.model, open(f"{self.output_file_base_path}-model.pkl", 'wb'))
        log.artifact(f"{self.output_file_base_path}-model.pkl")
        # model size in bytes
        model_size = get_file_size(f"{self.output_file_base_path}-model.pkl", SIZE_UNIT.BYTES)
        print(f"Model size is: {model_size} bytes")
        log.metric('model_size_in_bytes', model_size)

        return self

    def predict(self, x, threshold=None, prints=True):
        """
        Predict label of instance (domain vector representation).

        Arguments
        ---------
        x : str
            Domain vector representation.
        threshold : float, optional
            Threshold between [-1, 1] for further prediction manipulation.
                  -1 : nothing will every be evaluated as bad
                   1 : only 100% sure pages will be evaluated as top
                   0 : use model prediction default. Same as None only slower!
                None : same as 0. Only faster!
        prints : bool, optional
            Print warnings and further information.

        Returns
        -------
        str
            Predicted label for instance x.
        """
        X = [x]

        if threshold == None:
            [y] = self.model.predict(X)
            return y

        # with threshold
        # Note: probability=True is necessary
        [y_proba] = self.model.predict_proba(X)
        idx = 0 if self.model.classes_[0] == label['top'] else 1
        point = (y_proba[idx] - 50) * 2
        return label['bad'] if point < threshold else label['top']
