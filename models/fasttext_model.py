############################################################
# FastText operations.
############################################################

import fasttext
import json

import pandas as pd
from csv import QUOTE_NONE as CSV_QUOTE_NONE
from utils.file import convert_unit, get_file_size, SIZE_UNIT

from settings import label
from utils.tracking import Log as log
from models.base_model import BaseModel
from models.early_stopping import FastTextEarlyStopping


class FastTextModel(BaseModel):
    """
    FastText model to handle all FastText specific operations.
    """

    def __init__(self, output_file_base_path, output_file_base_path_surfix='fasttext', early_stopper=None):
        """
        Initialize FastText model.

        Arguments
        ---------
        output_file_base_path : str
            Directory path with possible file prefix to save files like model exports.
        output_file_base_path_surfix : str, optional
            Special file prefix additional to output_file_base_path.
        early_stopper : EarlyStoppingBase, optional
            Early stopping handler.
        """
        super().__init__(
            output_file_base_path + output_file_base_path_surfix,
            FastTextEarlyStopping() if early_stopper == None else early_stopper,
        )
        self.transformation_necessary = False

    def _write_data_to_fasttext_csv(X, y, csv_path):
        """
        Write FastText data csv.

        FastText can only handle files (not in-memory objects).
        Hence, at least training and validation data needs to be saved into a file.
        Note: FastText csv files has a special format.

        Arguments
        ---------
        X : array, np.ndarray or ndarray-like
            List of domains.
        y : array, np.ndarray or ndarray-like
            List of labels (according to the list of domains).
        csv_path : str
            CSV file path, where the data should be saved.
        """
        df = pd.DataFrame(zip(y, X), columns=['label','domain'])
        df[['label','domain']].to_csv(
            csv_path,
            sep=' ',
            index=False,
            header=False,
            quoting=CSV_QUOTE_NONE,
            escapechar=' ',
        )
        log.artifact(csv_path)

    def test(self, X_test, y_test, threshold=None, early_stopping=True):
        """
        Run the model test and export and print results.

        Arguments
        ---------
        X_test : array, np.ndarray or ndarray-like
            List of domains.
        y_test : array, np.ndarray or ndarray-like
            List of labels (according to the list of domains).
        threshold : float, optional
            Threshold between [-1, 1] for further prediction manipulation.
                  -1 : nothing will every be evaluated as bad
                   1 : only 100% sure pages will be evaluated as top
                   0 : use model prediction default. Same as None only slower!
                None : same as 0. Only faster!
        early_stopping : bool
            Early stopping activation state.
        """
        FastTextModel._write_data_to_fasttext_csv(X_test, y_test, f"{self.output_file_base_path}-test.txt")
        return super().test(X_test, y_test, threshold, early_stopping)

    def transform(self, domain_sentence):
        """
        Apply model transformations on a single prepared domain (domain sentence).

        Arguments
        ---------
        domain_sentence : str
            Prepared domain string (domain sentence).

        Returns
        -------
        float array
            Domain vector representation.
        """
        return self.model.get_word_vector(domain_sentence)

    def load(self, model_path=None):
        """
        Load a model given a filepath and return a model object.

        Arguments
        ---------
        model_path : str
            File path to exported model to load.

        Returns
        -------
        FastText
            FastText model.
        """
        if model_path == None:
            model_path = f"{self.output_file_base_path}-model.bin"

        self.model = fasttext.load_model(model_path)

        return self

    def train(self, X_train, y_train, X_validate, y_validate, train_params={}):
        """
        Train the model and export the result.

        Arguments
        ---------
        X_train : array, np.ndarray or ndarray-like
            List of training domains.
        y_train : array, np.ndarray or ndarray-like
            List of training labels (according to the list of training domains).
        X_validate : array, np.ndarray or ndarray-like
            List of validation domains.
        y_validate : array, np.ndarray or ndarray-like
            List of validation labels (according to the list of validation domains).
        train_params : dict, optional
            Further FastText supervised training arguments.

        Returns
        -------
        FastTextModel
            Self.
        """
        # create necessary fasttext data files
        train_file = f"{self.output_file_base_path}-train.txt"
        validate_file = f"{self.output_file_base_path}-validate.txt"
        FastTextModel._write_data_to_fasttext_csv(X_train, y_train, train_file)
        FastTextModel._write_data_to_fasttext_csv(X_validate, y_validate, validate_file)

        # FastText model training
        log.params(train_params)
        log.param('manual set training params', json.dumps(list(train_params.keys())))
        self.model = fasttext.train_supervised(
            input=train_file,
            autotuneValidationFile=validate_file,
            **train_params,
        )
        trained_params = vars(self.model).copy()
        [ trained_params.pop(key, None) for key in ['f', '_words', '_labels', 'label', 'verbose', 'pretrainedVectors'] ]
        [ trained_params.pop(key, None) for key in train_params.keys() ]
        log.params(trained_params)

        # save training parameters
        with open(f"{self.output_file_base_path}-config.json", 'w') as outfile:
            json.dump(train_params, outfile, indent=4, sort_keys=True)
        log.artifact(f"{self.output_file_base_path}-config.json")

        # save model
        self.model.save_model(f"{self.output_file_base_path}-model.bin")
        log.artifact(f"{self.output_file_base_path}-model.bin")
        # model size
        model_size = get_file_size(f"{self.output_file_base_path}-model.bin", SIZE_UNIT.BYTES)
        log.metric('model_size_in_bytes', model_size)
        log.metric('model_size_in_mb', convert_unit(model_size, to_unit=SIZE_UNIT.MB))
        model_size, size_unit = convert_unit(model_size)
        print(f"Model size is: {model_size} {str(size_unit)}")

        return self

    def predict(self, x, threshold=None, prints=True):
        """
        Predict label of instance (prepared domain string/domain sentence).

        Arguments
        ---------
        x : str
            Prepared domain string (domain sentence).
        threshold : float, optional
            Threshold between [-1, 1] for further prediction manipulation.
                  -1 : nothing will every be evaluated as bad
                   1 : only 100% sure pages will be evaluated as top
                   0 : use model prediction default. Same as None only slower!
                None : same as 0. Only faster!
        prints : bool, optional
            Print warnings and further information.

        Returns
        -------
        str
            Predicted label for instance x.
        """
        domain_sentence = x
        (predicted_label,), [probability] = self.model.predict(domain_sentence, k=1)

        if threshold == None or threshold == 0:
            return predicted_label

        if predicted_label == label['bad']:
            probability = -probability

        return label['bad'] if probability < threshold else label['top']
