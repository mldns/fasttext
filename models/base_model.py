############################################################
# Model base.
############################################################

from utils.tracking import Log as log

import json

from sklearn.metrics import classification_report

from settings import label
from utils.log_error import log_error


class BaseModel:
    """
    Base class for all models.
    """

    def __init__(self, output_file_base_path, early_stopper=None):
        """
        Initialize model.

        Arguments
        ---------
        output_file_base_path : str
            Directory path with file prefix to save files like model exports.
        early_stopper : EarlyStoppingBase, optional
            Early stopping handler.
        """
        self.output_file_base_path = output_file_base_path
        self.early_stopper = early_stopper

    def test(self, X_test, y_test, threshold=None, early_stopping=True):
        """
        Run the model test and export and print results.

        Arguments
        ---------
        X_test : array, np.ndarray or ndarray-like
            List of domains.
        y_test : array, np.ndarray or ndarray-like
            List of labels (according to the list of domains).
        threshold : float, optional
            Threshold between [-1, 1] for further prediction manipulation.
                  -1 : nothing will every be evaluated as bad
                   1 : only 100% sure pages will be evaluated as top
                   0 : use model prediction default. Same as None only slower!
                None : same as 0. Only faster!
        early_stopping : bool, optional
            Early stopping activation state.
        """
        first = True
        predictions = []
        for x in X_test:
            if self.transformation_necessary:
                x = self.transform(x)
            y = self.predict(x, threshold, prints=first)
            predictions.append(y)
            first = False

        result = self._analyse_test(y_test, predictions)
        self._print(result)
        self._early_stopping_evaluation(result, early_stopping=early_stopping)

    def _early_stopping_evaluation(self, result, early_stopping=True):
        """
        Evaluate early stopping condition and stop the program if necessary.

        Arguments
        ---------
        result : dict
            Test results from sklearn.metrics.classification_report as dict.
        early_stopping : bool, optional
            Early stopping activation state.
        """
        if self.early_stopper == None:
            return

        if early_stopping and self.early_stopper.evaluate(result):
            log_error('ERROR: Early Stopping!', self.output_file_base_path)
            exit(-1)

    def _analyse_test(self, y_test, predictions):
        """
        Analyse test and export results.

        Arguments
        ---------
        y_test : array, np.ndarray or ndarray-like
            List of actual labels.
        predictions : array, np.ndarray or ndarray-like
            List of predicted labels.

        Returns
        -------
        dict
            Test results from sklearn.metrics.classification_report as dict.
        """
        result = classification_report(
            y_test,
            predictions,
            output_dict=True,
            digits=4,
            target_names=[label['top'],label['bad']],
        )
        log.metrics({
            'accuracy':      result["accuracy"],
            'bad_precision': result[label['bad']]["precision"],
            'bad_recall':    result[label['bad']]["recall"],
            'top_precision': result[label['top']]["precision"],
            'top_recall':    result[label['top']]["recall"],
        })

        with open(f"{self.output_file_base_path}-test.json", 'w') as outfile:
            json.dump(result, outfile, indent=4, sort_keys=True)
        log.artifact(f"{self.output_file_base_path}-test.json")

        return result

    def _print(self, result):
        """
        Print test results (short) to console.

        Arguments
        ---------
        result : dict
            Test results from sklearn.metrics.classification_report as dict.
        """
        print(f"Recall (Top): {str(result[label['top']]['recall'])[:6]}")
        print(f"Recall (Bad): {str(result[label['bad']]['recall'])[:6]}")
        print(f"    Accuracy: {str(result['accuracy'])[:6]}")

    def _pretty_print(self, result):
        """
        Pretty print test results to console.

        Arguments
        ---------
        result : dict
            Test results from sklearn.metrics.classification_report as dict.
        """
        print(result)
