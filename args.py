#!/usr/bin/env python3
############################################################
# Parse training arguments.
############################################################

from argparse import ArgumentParser
from utils.argparser_ext import directory, file, StoreDictKeyPair, cls_method

import time

from data.preprocessing import DataPreprocessing
from data.scaling import DataScaling
from data.selection import DataSelection


def parseArgs():
    """
    Parse command-line arguments.

    Returns
    -------
    Namespace
        Run configuration.
        Converted argument strings as object with matching attributes.
    """
    parser = ArgumentParser(description="Train models.")

    # Output Settings #####################################
    timestr = time.strftime("%Y%m%d-%H%M%S")
    parser.add_argument(
        '--out',
        type=directory(exist=False),
        default=f"./out/{timestr}",
        help='output directory [Default: "./out/yyyymmdd-hhmmss"]',
    )
    parser.add_argument(
        '--out-name',
        type=str,
        default='',
        help='output base filename prefix',
    )
    parser.add_argument(
        '--seed',
        type=int,
        default=None,
        help='Set fix seed instead of random seed.',
    )

    # Data ################################################
    parser.add_argument(
        '--data-selection',
        type=cls_method(DataSelection),
        default=DataSelection.whitelist_blacklist,
        help='Data selection steps [Default: whitelist_blacklist]',
    )
    parser.add_argument(
        '--data-preprocessing',
        type=cls_method(DataPreprocessing),
        nargs='+',
        default=DataPreprocessing.lower_case_split_by_dot_dash_underscore,
        help='Data preprocessing steps [Default: lower_case_split_by_dot_dash_underscore]',
    )
    # Data Source/Base
    parser.add_argument(
        "--whitelist_path",
        type=str,
        default="./datasets/whitelist_cleaned.txt",
        help="Path to the whitelist file [default: %(default)s]",
    )
    parser.add_argument(
        "--blacklist_path",
        type=str,
        default="./datasets/blacklist.txt",
        help="Path to the whitelist file [default: %(default)s]",
    )
    parser.add_argument(
        "--dga_path",
        type=str,
        default="./datasets/dgas_preprocessed.csv",
        help="Path to the DGA file [default: %(default)s]",
    )

    # Model Training Settings #############################
    # Set flag which models should be trained.
    # Optional: Use specific training parameters.
    parser.add_argument(
        '--fasttext',
        action=StoreDictKeyPair,
        nargs='*',
        default=None,
        help='FastText training parameters',
    )
    parser.add_argument(
        '--pca',
        action=StoreDictKeyPair,
        nargs='*',
        default=None,
        help='PCA training parameters',
    )
    parser.add_argument(
        '--svm',
        action=StoreDictKeyPair,
        nargs='*',
        default=None,
        help='SVM training parameters',
    )

    # Preloaded Models ####################################
    # Use already trained models.
    parser.add_argument(
        '--fasttext-model',
        type=file,
        default=None,
        help='path to fasttext model file',
    )
    parser.add_argument(
        '--pca-model',
        type=file,
        default=None,
        help='path to pca model file',
    )
    parser.add_argument(
        '--svm-model',
        type=file,
        default=None,
        help='path to svm model file',
    )

    # Thresholds ##########################################
    # Thresholds range [-1, 1]
    #   -1 : nothing will every be evaluated as bad
    #    1 : only 100% sure pages will be evaluated as top
    #    0 : use model prediction default. Same as None only slower!
    # None : same as 0. Only faster!
    parser.add_argument(
        '--fasttext-threshold',
        type=float,
        default=None,
        help='Fasttext threshold [Default: None]',
    )
    parser.add_argument(
        '--pca-threshold',
        type=float,
        default=None,
        help='PCA threshold [Default: None]',
    )
    parser.add_argument(
        '--svm-threshold',
        type=float,
        default=None,
        help='SVM threshold [Default: None]',
    )

    # Scaling #############################################
    parser.add_argument(
        '--fasttext-word-vector-scaling',
        type=cls_method(DataScaling),
        default=DataScaling.no_scaling,
        help='Scale word vectors/FastText output [Default: no_scaling]',
    )
    parser.add_argument(
        '--pca-output-scaling',
        type=cls_method(DataScaling),
        default=DataScaling.no_scaling,
        help='Scale PCA output [Default: no_scaling]',
    )

    # parse
    config = parser.parse_args()

    config.output_file_base_path = f"{config.out}/{config.out_name}"

    if not isinstance(config.data_preprocessing, list):
        config.data_preprocessing = [ config.data_preprocessing ]

    config.fasttext = True if config.fasttext_model else config.fasttext
    config.pca = True if config.pca_model else config.pca
    config.svm = True if config.svm_model else config.svm

    return config
