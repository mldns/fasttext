#!/usr/bin/env python3
############################################################
# Analyse backlists to check if adware is contained.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

import pandas as pd

from scripts.download_and_prepare_white_and_blacklist import load_blacklists, download_toulouse_list, prepare_df


def load_not_malicious_list():
    """
    Load blacklist data frame without malicious domains but other bad domain categories.

    Returns
    -------
    pd.DataFrame
        Blacklist data frame without malicious domains.
    """
    categories = [
        ('ads', 'publicite'),
        ('adult', 'adult'),
        ('aggressive', 'agressif'),
        ('porn', 'adult'),
        ('sect', 'sect'),
        ('violence', 'agressif'),
    ]

    dfs = []
    for category, archive_path in categories:
        dfs.append(download_toulouse_list(category, archive_path))

    # concat and prepare
    df = pd.concat(dfs, ignore_index=True)
    df = prepare_df(df)
    return df


def main():
    """
    Main entry for the analyse backlists to check if adware is contained script.
    """
    bad_dfs = load_blacklists()

    # blacklist: COVID-19 Cyber Threat Coalition
    df = bad_dfs[0]
    df = df['domain']
    print(len(df))

    # not malicious list
    nml_df = load_not_malicious_list()
    nml_df = nml_df['domain']

    # remove toulouse list (maleware, phishing)
    nml_df = nml_df[~nml_df.isin(bad_dfs[2]['domain'])].dropna()
    nml_df = nml_df[~nml_df.isin(bad_dfs[3]['domain'])].dropna()

    # compare COVID-19 Cyber Threat Coalition with not malicious list
    intersec_df = df[df.isin(nml_df)]
    intersec_df = intersec_df.reset_index()['domain']

    # show
    print(intersec_df)
    print(len(intersec_df))


if __name__ == '__main__':
    main()
