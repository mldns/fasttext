#!/usr/bin/env python3
############################################################
# Get the first and second level domain.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

from tld import get_fld



def first_level(domain):
    """
    Get the first level domain (fld).
    Definition fld example: google.com

    Arguments
    ---------
    domain : str
        Domain name.

    Returns
    -------
    str
        First level domain of the given domain name.
    """
    return get_fld(domain, fix_protocol=True, fail_silently=True)


def second_level(domain):
    """
    Get the second level domain (sld).
    Definition sld example: maps.google.com

    Arguments
    ---------
    domain : str
        Domain name.

    Returns
    -------
    str
        Second level domain of the given domain name.
    """
    first = first_level(domain)
    if first is None:
        return None
    n = len(first.split('.'))
    return '.'.join(domain.split('.')[-(n + 1):])
