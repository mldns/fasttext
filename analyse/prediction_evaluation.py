#!/usr/bin/env python3
############################################################
# Prediction Evaluation.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

from os import path, system
from pickle import load
from argparse import ArgumentParser

from utils.argparser_ext import file
from scripts.create_model_plots import load_models
from settings import label

from executor import Executor


def predict(domain, fasttext, pca, fasttext_config):
    """
    Predict label and value.

    Arguments
    ---------
    domain : str
        Domain name.
    fasttext : object
        Fasttext model
    pca : object
        PCA model (n_components=1)
    fasttext_config : dict
        FastText Configuration

    Returns
    -------
    (str, float), (str, float)
        label and value tuple; first from FastText only and second from FastText and PCA
    """
    [[x]] = Executor.run_pipeline(fasttext_config.data_preprocessing, [domain])

    # FastText
    (ft_predicted_label,), [ft_probability] = fasttext.predict(x, k=1)
    ft_probability = ft_probability if ft_predicted_label == label['top'] else -ft_probability

    # PCA
    sentence_vector = [fasttext.get_sentence_vector(x)]
    _, sentence_vector = fasttext_config.fasttext_word_vector_scaling(sentence_vector)
    [pca_value] = pca.transform(sentence_vector)[0]
    pca_predicted_label = label['bad'] if pca_value < 0 else label['top']

    return (ft_predicted_label, ft_probability), (pca_predicted_label, pca_value)


def main(config):
    """
    Main entry for the evaluation script.

    Arguments
    ---------
    config : Namespace
        Run configuration.
        Converted argument strings as object with matching attributes.
    """
    fasttext, pca = load_models(config.fasttext, config.pca)
    fasttext_config = load(open(config.fasttext_config, 'rb'))

    print('→ analyse')
    with open(config.data, 'r') as i, open(f"{config.output}-fasttext.csv", 'w') as ft, open(f"{config.output}-pca.csv", 'w') as p:
        for idx, domain in enumerate(i):
            domain = domain.rstrip()
            (ft_label, ft_value), (pca_label, pca_value) = predict(domain, fasttext, pca, fasttext_config)
            ft_value = '%.16f' % ft_value
            pca_value = '%.16f' % pca_value
            ft.write(f"{ft_label},{ft_value},{domain}\n")
            p.write(f"{pca_label},{pca_value},{domain}\n")
            print(f"\r{idx}", end='')
    print()

    print('→ sort')
    system(f"sort --parallel {config.parallel} -t, -k 2n -o '{config.output}-fasttext.csv' '{config.output}-fasttext.csv'")
    system(f"sort --parallel {config.parallel} -t, -k 2n -o '{config.output}-pca.csv' '{config.output}-pca.csv'")


def parseArgs():
    """
    Parse command-line arguments.

    Returns
    -------
    Namespace
        Run configuration.
        Converted argument strings as object with matching attributes.
    """
    parser = ArgumentParser(description='Prediction Evaluation')

    parser.add_argument(
        '--data',
        type=file,
        default=path.abspath(path.join(path.dirname(__file__), '../datasets/2021-06-10_ipv4_preprocessed')),
        help='Path to file with list of domain names to predict.',
    )

    parser.add_argument(
        '--output',
        type=str,
        default=path.abspath(path.join(path.dirname(__file__), '../datasets/2021-06-10_ipv4_preprocessed_prediction_evaluation.csv')),
        help='Path to output file.',
    )

    parser.add_argument(
        '--fasttext',
        type=file,
        help='Path to FastText model file.',
    )

    parser.add_argument(
        '--fasttext-config',
        type=file,
        default=None,
        help='Path to FastText config file.',
    )

    parser.add_argument(
        '--pca',
        type=file,
        default=None,
        help='Path to PCA model file.',
    )

    parser.add_argument(
        '--parallel',
        type=int,
        default=4,
        help='Number of threads.',
    )


    config = parser.parse_args()

    if config.fasttext_config is None:
        config.fasttext_config = path.join(path.dirname(config.fasttext), 'config.pkl')

    return config


if __name__ == '__main__':
    main(parseArgs())
