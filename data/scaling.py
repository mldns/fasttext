############################################################
# Collection of methods to scale datasets.
############################################################

from sklearn.preprocessing import Normalizer, StandardScaler
from sklearn.base import TransformerMixin


class DataScaling:
    """
    Collection of methods to scale datasets.
    """

    def _scale(Scaler, X_train, X_validate=None, X_test=None):
        """
        Transform data by specific scaler.

        Arguments
        ---------
        Scaler : TransformerMixin
            Transformer instance to scale the data.
        X_train : array-like
            List of training domains.
        X_validate : array-like, optional
            List of validation domains.
        X_test : array-like, optional
            List of testing domains.

        Return
        ------
        TransformerMixin, array-like [, array-like [, array-like]]
            Transfomer and the transformed arguments.
        """
        scaler = Scaler()

        X_train_scaled = scaler.fit_transform(X_train)
        if X_validate == None:
            return scaler, X_train_scaled

        X_validate_scaled = scaler.fit_transform(X_validate)
        if X_test == None:
            return scaler, X_train_scaled, X_validate_scaled

        X_test_scaled = scaler.fit_transform(X_test)
        return scaler, X_train_scaled, X_validate_scaled, X_test_scaled

    # PUBLIC ##############################################

    def no_scaling(X_train, X_validate=None, X_test=None):
        """
        This scaling method does nothing but returns a NoScaling instance
        and the original input.

        Arguments
        ---------
        X_train : array-like
            List of training domains.
        X_validate : array-like, optional
            List of validation domains.
        X_test : array-like, optional
            List of testing domains.

        Return
        ------
        TransformerMixin, array-like [, array-like [, array-like]]
            Transfomer and the transformed arguments.
        """
        return DataScaling._scale(NoScaling, X_train, X_validate, X_test)

    def normalizer(X_train, X_validate=None, X_test=None):
        """
        Normalize samples individually to unit norm.

        Arguments
        ---------
        X_train : array-like
            List of training domains.
        X_validate : array-like, optional
            List of validation domains.
        X_test : array-like, optional
            List of testing domains.

        Return
        ------
        TransformerMixin, array-like [, array-like [, array-like]]
            Transfomer and the transformed arguments.
        """
        return DataScaling._scale(Normalizer, X_train, X_validate, X_test)

    def standard_scaler(X_train, X_validate=None, X_test=None):
        """
        Standardize features by removing the mean and scaling to unit variance.

        Arguments
        ---------
        X_train : array-like
            List of training domains.
        X_validate : array-like, optional
            List of validation domains.
        X_test : array-like, optional
            List of testing domains.

        Return
        ------
        TransformerMixin, array-like [, array-like [, array-like]]
            Transfomer and the transformed arguments.
        """
        return DataScaling._scale(StandardScaler, X_train, X_validate, X_test)


class NoScaling(TransformerMixin):
    """
    No scaling class.
    Scaling class which does nothing but return the original input.
    """

    def fit(self, X, y=None, **fit_params):
        """
        Normally fits the transformer to data.
        But here it does nothing.

        Arguments
        ---------
        X : array-like
            List of domain names.
        y : array-like, optional
            List of labels (according to the list of domains).
        **fit_params : dict, optional
            Additional fit parameters.

        Returns
        -------
        NoScaling
            Self.
        """
        return self

    def transform(self, X):
        """
        Normally transforms the data.
        But here it does nothing.

        Arguments
        ---------
        X : array-like
            List of domain names.

        Returns
        -------
        array-like
            Normally transformed domain names. Here the domain names keep unchanged.
        """
        return X
