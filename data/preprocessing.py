############################################################
# Collection of methods to prepare a single domain name.
############################################################

from tld import get_tld


class DataPreprocessing:
    """
    Collection of methods to prepare a single domain name.
    """

    def _domain_level_n(domain, n):
        """
        Get domain name with maximum number of levels.

        Arguments
        ---------
        domain : str
            Domain name.
        n : int
            Maximum level number.

        Returns
        -------
        str
            Domain name with a maximum of n levels.

        Examples
        --------
        _domain_level_n('maps.google.com', 1)  →  'com'
        _domain_level_n('maps.google.com', 2)  →  'google.com'
        _domain_level_n('maps.google.com', 9)  →  'maps.google.com'
        """
        tld = get_tld(f"http://{domain}", fix_protocol=True, fail_silently=True)
        if tld is None:
            parts = domain.split('.')
        else:
            parts = domain.rstrip(f".{tld}").split('.') + [ tld ]

        return '.'.join(parts[-n:])

    def _max_length_n(domain, n):
        """
        Get domain name with maximum number of characters.

        Arguments
        ---------
        domain : str
            Domain name.
        n : int
            Maximum number of characters.

        Returns
        -------
        str
            Domain name with a maximum of n characters.

        Examples
        --------
        _max_length_n('maps.google.com', 1)    →  'm'
        _max_length_n('maps.google.com', 10)   →  'google.com'
        _max_length_n('maps.google.com', 100)  →  'maps.google.com'
        """
        return domain[:n]

    def _max_word_length_n(domain_sentense, n, sep):
        """
        Split each word of the domain sentence if the maximum number of charaters is reached.

        Arguments
        ---------
        domain_sentense : str
            Domain sentence.
        n : int
            Maximum number of characters per word.
        sep : str
            Single character as word seperator.

        Returns
        -------
        str
            Domain sentence with each word with a maximum of n characters.

        Examples
        --------
        _max_word_length_n('maps google com', 1)  →  'm a p s g o o g l e c o m'
        _max_word_length_n('maps google com', 2)  →  'ma ps go og le co m'
        _max_word_length_n('maps google com', 9)  →  'maps google com'
        """
        return sep.join([x[i:i+n] for x in domain_sentense.split(sep) for i in range(0, len(x), n)])

    # PUBLIC ##############################################

    def lower_case_split_by_dot_dash_underscore(domain):
        """
        Apply to domain name:
        - characters are lower case
        - replace dots, dashes and underscores with a single space.

        Arguments
        ---------
        domain : str
            Domain name.

        Return
        ------
        str
            Domain sentence. Manipulated domain name.

        Examples
        --------
        lower_case_split_by_dot_dash_underscore('aBc-dEf_gHi.jK')  →  'abc def ghi jk'
        """
        return domain.lower() \
            .replace('.', ' ').replace('-', ' ').replace('_', ' ')

    def trim_tailing_dot(domain):
        """
        Remove tailing dots.

        Arguments
        ---------
        domain : str
            Domain name.

        Return
        ------
        str
            Domain name without tailing dots.

        Examples
        --------
        trim_tailing_dot('translate.google.com.')  →  'translate.google.com'
        """
        return domain.rstrip('.')

    def trim_leading_www(domain):
        """
        Remove leading 'www.'.

        Arguments
        ---------
        domain : str
            Domain name.

        Return
        ------
        str
            Domain name without tailing leading 'www.'.

        Examples
        --------
        trim_leading_www('www.google.com')  →  'google.com'
        """
        return domain.lstrip('www')

    def domain_level_2(domain):
        """
        Get domain name with only first and top level domain.

        Arguments
        ---------
        domain : str
            Domain name.

        Return
        ------
        str
            Domain name with only first and top level domain.

        Examples
        --------
        domain_level_2('sub3.sub2.sub1.abc.xx')  →  'abc.xx'
        """
        return DataPreprocessing._domain_level_n(domain, 2)

    def domain_level_3(domain):
        """
        Get domain name with only one subdomain additional to first and top
        level domain.

        Arguments
        ---------
        domain : str
            Domain name.

        Return
        ------
        str
            Domain name with only one subdomain additional to first and top
            level domain.

        Examples
        --------
        domain_level_2('sub3.sub2.sub1.abc.xx')  →  'sub1.abc.xx'
        """
        return DataPreprocessing._domain_level_n(domain, 3)

    def max_length_30(domain):
        """
        Cut domain name to a maximum length of 30 characters.

        Arguments
        ---------
        domain : str
            Domain name.

        Returns
        -------
        str
            Domain name with a maximum of 30 characters.

        Examples
        --------
        max_length_30('sub3.sub2.sub1.abc.[...].xx')  →  'abc.[...].xx' with len('abc.[...].xx') == 30
        """
        return DataPreprocessing._max_length_n(domain, 30)

    def max_length_40(domain):
        """
        Cut domain name to a maximum length of 40 characters.

        Arguments
        ---------
        domain : str
            Domain name.

        Returns
        -------
        str
            Domain name with a maximum of 40 characters.

        Examples
        --------
        max_length_40('sub3.sub2.sub1.abc.[...].xx')  →  'abc.[...].xx' with len('abc.[...].xx') == 40
        """
        return DataPreprocessing._max_length_n(domain, 40)

    def max_length_50(domain):
        """
        Cut domain name to a maximum length of 50 characters.

        Arguments
        ---------
        domain : str
            Domain name.

        Returns
        -------
        str
            Domain name with a maximum of 50 characters.

        Examples
        --------
        max_length_50('sub3.sub2.sub1.abc.[...].xx')  →  'abc.[...].xx' with len('abc.[...].xx') == 50
        """
        return DataPreprocessing._max_length_n(domain, 50)

    def max_length_100(domain):
        """
        Cut domain name to a maximum length of 100 characters.

        Arguments
        ---------
        domain : str
            Domain name.

        Returns
        -------
        str
            Domain name with a maximum of 100 characters.

        Examples
        --------
        max_length_100('sub3.sub2.sub1.abc.[...].xx')  →  'abc.[...].xx' with len('abc.[...].xx') == 100
        """
        return DataPreprocessing._max_length_n(domain, 100)

    def max_word_length_1(domain_sentence):
        """
        Set each character a own word.

        Arguments
        ---------
        domain_sentense : str
            Domain sentence.

        Returns
        -------
        str
            Domain sentence with each character as word.

        Examples
        --------
        max_word_length_1('maps google com')  →  'm a p s g o o g l e c o m'
        """
        return DataPreprocessing._max_word_length_n(domain_sentence, 1, ' ')

    def max_word_length_3(domain_sentence):
        """
        Split each word of the domain sentence every 3 characters in to a
        seperated word.

        Arguments
        ---------
        domain_sentense : str
            Domain sentence.

        Returns
        -------
        str
            Domain sentence with each word with a maximum of 3 characters.

        Examples
        --------
        max_word_length_n('maps google com')  →  'map s goo gle com'
        """
        return DataPreprocessing._max_word_length_n(domain_sentence, 3, ' ')

    def max_word_length_5(domain_sentence):
        """
        Split each word of the domain sentence every 5 characters in to a
        seperated word.

        Arguments
        ---------
        domain_sentense : str
            Domain sentence.

        Returns
        -------
        str
            Domain sentence with each word with a maximum of 5 characters.
        """
        return DataPreprocessing._max_word_length_n(domain_sentence, 5, ' ')

    def max_word_length_10(domain_sentence):
        """
        Split each word of the domain sentence every 10 characters in to a
        seperated word.

        Arguments
        ---------
        domain_sentense : str
            Domain sentence.

        Returns
        -------
        str
            Domain sentence with each word with a maximum of 10 characters.
        """
        return DataPreprocessing._max_word_length_n(domain_sentence, 10, ' ')

    def remove_tld(domain):
        """
        Remove top level domain from domain name.

        Arguments
        ---------
        domain : str
            Domain name.

        Returns
        -------
        str
            Domain name without top level domain.

        Examples
        --------
        remove_tld('maps.google.com')  →  'maps.google'
        """
        tld = get_tld(f"http://{domain}", fix_protocol=True, fail_silently=True)
        if tld is None:
            return domain
        return domain.rstrip(f".{tld}")
